#!/bin/bash
#$ -pe smp 1
#$ -q all.q
#$ -cwd
#$ -S /bin/bash

perl /isilon/biodiversity/users/lowec/metagenomics/AODP-OFP/scripts_repo/Download_Seqs/downloadSeqsFromGenBank.pl -query regulated_genus.query -email Your_address@agr.gc.ca -out .
