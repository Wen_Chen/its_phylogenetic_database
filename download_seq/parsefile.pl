#!/opt/perl/bin/perl
use strict;
use warnings;
use Spreadsheet::ParseExcel;

my %genusName;
my $parser = Spreadsheet::ParseExcel->new();
my $workbook = $parser->Parse("fungal_pathogens.xls");
my $worksheet = $workbook->Worksheet(0);
my ($min, $max) = $worksheet->RowRange();
for my $row ( 2 .. $max){
	my $cell = $worksheet->Cell($row, 1);
	my $value = ($cell ? $cell->value() : "");
	my ($genus, $species) = split(' ', $value, 2);
	if( defined $genus ){
		if( exists $genusName{$genus} ){
                       my $t = $genusName{$genus};
                       push( @$t, $species );
                       $genusName{$genus} = $t;
		}else{
                	my $array = [];
                   	push (@$array, $species);
                   	$genusName{$genus} = $array;
			mkdir "$genus";
		}

	}
}
for my $key ( sort keys %genusName ) {
	my $value = $genusName{$key};
	print "$key => " . @$value[0] . " \n ";
	my $fileName = "$key/species.txt";
	open(my $fh, '>', $fileName) or die "could not open file";
	foreach(@$value){
		print $fh "$key $_\n";
	}
	close $fh;
}

