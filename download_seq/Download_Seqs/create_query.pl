#!/usr/bin/env perl
use strict;
use warnings;
use utf8;
use FindBin;
use lib "$FindBin::Bin/Modules/Bio-EUtilities-1.73/lib";
use Bio::DB::EUtilities;
use Bio::SeqIO;
use Cwd;
use Getopt::Long;

## an extremely simple script to create queries for each taxon name
## in a plain text file

## VARIABLES
my $taxaname=shift or die "please provide a plain text file with multiple taxon names";
my $query_string=shift or die "sequence features to be searched and downloaded from genbank; e.g. '[organism] AND (5.8S OR ITS1 OR ITS2 OR ITS) NOT (complete AND genome) NOT (chromosome)'";   
my $out_file=shift;     



open (OUT, ">$out_file");
open (FH, "<$taxaname");
while (<FH>) {
  chomp $_;
  if ($_!~ m/^$/) {
    print $_, "\n";
    my $key = $_;
    $key =~ s/[\s\W]+/_/g; #\s matches any whitespace character; \W: matches any non-“word” character
    print $key, "\n"; 
    print OUT $key, "\t", join($_, " ", $query_string), "\n";
  }
}
close FH;
close OUT;

