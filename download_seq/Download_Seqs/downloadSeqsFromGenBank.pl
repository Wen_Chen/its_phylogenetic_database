#!/usr/bin/env perl
use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/Modules/Bio-EUtilities-1.73/lib";
use Bio::DB::EUtilities;
use Bio::SeqIO;
use Getopt::Long;

## BIO::DB::EUtilities is not included in BioPerl.
## The latest release can be downloaded from cpan.
## The reason this module is used is because there is a limit of 
##   10,000 sequences that can be downloaded for a single query
##   with Bio::DB::GenBank.

## The downside is that there are connection issues, whose error 
##   messages sometimes end up being printed into the output file.

## VARIABLES
my $query_file;           # see example query file for format; first column is used as the name of the fasta file created
my $out_folder;           # output folder where the downloaded files are created
my $download_type;        # can be 'fasta', 'gb', 'gi', etc
my $summary_file; 	  # summary of the downloaded sequences
my $email;                # required per Bio::DB::EUtilities policy
my $err_file;		  # file where error messages are printed in; removed if none printed
my $not_found_file;	  # file where the id & query with no sequences found are printed
my $summary_header = "QUERY_ID\tNUM_SEQS_DOWNLOADED";
my $err_help = "To see help doc, use command: \"perl downloadSeqsFromGenBank.pl -h\"";


## SETUP
&setup;
print "Query file: $query_file\n";
print "Output directory: $out_folder\n";
print "Download type: $download_type\n";
print "Summary output path: $summary_file\n";
print "Email: $email\n";
print "Error output path: $err_file\n";
print "Query not found output path: $not_found_file\n";

print "START: " . localtime . "\n";

open (READ, "<", $query_file) or die $!;
open (SUM, ">", $summary_file) or die $!;
open (NOT_FOUND, ">", $not_found_file) or die $!;
print SUM $summary_header, "\n";
open(my $err_out, ">", $err_file) or die $!;

## DOWNLOAD
while (<READ>){
  chomp;
  my $key = (split(/\t/, $_))[0];
  my $query = (split(/\t/, $_))[1];
  my $total;
  print "\nDownloading: $key...\n";
  my $out_file = "$out_folder/$key.$download_type";
  do{
    local *STDERR = $err_out;
    my $factory;
    if ($email ne ''){
      $factory = Bio::DB::EUtilities->new(-eutil      => 'esearch',
                                          -db         => 'nucleotide',
                                          -term       => $query,
                                          -usehistory => 'y',
                                          -email      => $email);
    } else {
      $factory = Bio::DB::EUtilities->new(-eutil      => 'esearch',
                                          -db         => 'nucleotide',
                                          -term       => $query,
                                          -usehistory => 'y');
    }
    $total = $factory->get_count;
    if (! defined $total){
      $total = 0;
    }
    # get history from queue
    my $hist  = $factory->next_History || die 'No history data returned';
    $factory->set_parameters(-eutil   => 'efetch',
                             -rettype => $download_type,
                             -history => $hist);

    my $retry = 0;
    my ($retmax, $retstart) = (500,0);
    open (OUT, '>', $out_file) or die $!;

    RETRIEVE_SEQS:
    while ($retstart < $total) {
        $factory->set_parameters(-retmax   => $retmax,
                                 -retstart => $retstart);
        eval{
            $factory->get_Response(-cb => sub {my ($data) = @_; print OUT $data} );
        };
        if ($@) {
            die "Server error: $@.  Try again later" if $retry == 5;
            print STDERR "Server error, redo #$retry\n";
            $retry++ && redo RETRIEVE_SEQS;
        }
        print "Retrieved $retstart...\n";
        $retstart += $retmax;
    }
    close OUT;

  };

  my $downloaded;
  if ($download_type eq 'gi'){
    $downloaded = `wc -l < \"$out_file\"`;
  } elsif ($download_type eq 'fasta'){
    $downloaded = `grep \">\" \"$out_file\" | wc -l`;
  } elsif (($download_type eq 'gb') ||
           ($download_type eq 'gbwithparts')){
    $downloaded = `grep \"LOCUS\" \"$out_file\" | wc -l`;
  } else {
    $downloaded = "n\\a";
  }
  chomp $downloaded;
  print "The total number of sequences found in GenBank is: $total\n";
  print "The total number of sequences downloaded is: $downloaded\n";
  if (($downloaded ne 'n\a') && 
      ($total ne $downloaded)){
    print "Warning: The totally number of sequences found in GenBank is not the same as the total number downloaded. Re-downloading...\n";
    close OUT;
    redo;
  } elsif ($total eq '0'){
    print "No sequence downloaded. Deleting output sequence file...\n";
    system("rm \"$out_file\"");
    print SUM $key, "\t", $downloaded, "\n";
    print NOT_FOUND $key, "\t", $query, "\n";
    next;
  }
  if ($download_type eq 'fasta'){
    &fastaToFasta("$out_file", "$out_file.tmp");
    system("mv \"$out_file.tmp\" \"$out_file\"");
  }
  print SUM $key, "\t", $downloaded, "\n";
  close OUT;
}

close READ;
close SUM;
close NOT_FOUND;
close $err_out;

if (! -z $err_file){
  print "\nError messages captured and printed.\n";
  print "    See output file: $err_file\n"; 
} else {
  print "\nNo error message captured.\n";
}
if (! -z $not_found_file){
  print "No sequences were found in GenBank for some query entries.\n";
  print "    See output file: $not_found_file\n";
} else {
  print "Sequences were found in GenBank for all query entries.\n";
}


## Print error message to err.txt if it exists

print "\nEND: " . localtime . "\n";


# SETUP OPTIONS AND DEFAULT VALUES
sub setup{
  my $help;
  GetOptions("--help" => \$help, 
             "-help" => \$help, 
             "-h" => \$help, 
             "-query=s" => \$query_file,
             "-out=s" => \$out_folder,
             "-type=s" => \$download_type,
             "-summary=s" => \$summary_file,
             "-not_found=s" => \$not_found_file,
             "-error=s" => \$err_file,
             "-email=s" => \$email) or die $!;
  if (defined $help){
    # prints out the help doc
    system("cat $FindBin::Bin/help_downloadSeqsFromGenBank.txt");
    exit;
  }

  my $err = 0;

  ## Checking if mandatory fields are given
  if (! defined $query_file){
    print "Error: query file path is not provided.\n";
    $err = 1;
  } elsif (! -e $query_file){
    print "Error: the provided query file path does not exist.\n";
    print "    Input: $query_file\n";
    $err = 1;
  }
  if (! defined $out_folder){
    print "Error: output directory path is not provided.\n";
    $err = 1;
  } elsif (! -d $out_folder){
    print "Error: the provided output directory path does not exist.\n";
    print "    Input: $out_folder\n";
    $err = 1;
  }
  # If input errors found, then exit script
  if ($err){
    print "($err_help)\n";
    exit;
  }

  ## Default values
  if (! defined $download_type){
    $download_type = 'fasta';
  }
  if (! defined $summary_file){
    $summary_file = "$out_folder/summary.tab";
  }
  if (! defined $email){
    $email = '';
  }
  if (! defined $err_file){
    $err_file = "$out_folder/error.txt";
  }
  if (! defined $not_found_file){
    $not_found_file = "$out_folder/not_found.tab";
  }


}

## Re-format the fasta file (i.e. nucleotide bases on one line)
sub fastaToFasta{
  my $in_file = shift or die $!;
  my $out_file = shift or die $!;
  my $seqio = Bio::SeqIO->new(-file => $in_file, -format => 'fasta' );
  open(OUT, ">", $out_file) or die $!;
  while(my $seq = $seqio->next_seq){
    print OUT ">", $seq->display_id, 
              " ", $seq->desc, 
              "\n", $seq->seq, "\n";
  }
  close OUT;
}
