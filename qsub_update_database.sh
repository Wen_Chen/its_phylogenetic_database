#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Update_database"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";

date=$(tail -2 $oligo/update_date_log | head -1);

if [ ! -d "$oligo/Old_version" ]; then
        mkdir $oligo/Old_version;
fi

if [ -d "$oligo/Processed" ]; then
	
	mkdir $oligo/Old_version/$date;
	
	mv $oligo/Processed/ $oligo/Old_version/$date/;
	cd $oligo/Old_version;
	
	tar -czvf "$date".tar.gz $date;
	
	rm -rf $oligo/Old_version/$date;
fi

if [ -d "$oligo/raw" ]; then
	mv $oligo/raw $oligo/Processed/;
fi

ssh peterzhu@soilbiome.gis.agr.gc.ca "sed -i -e 's/\r$//' /var/www/html/aodp/update.pl";
sleep 5;
ssh peterzhu@soilbiome.gis.agr.gc.ca "chmod 0777 /var/www/html/aodp/*";
sleep 5;
ssh peterzhu@soilbiome.gis.agr.gc.ca "/var/www/html/aodp/update.pl $oligo";
sleep 5;
