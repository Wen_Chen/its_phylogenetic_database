#!/bin/bash

echo -e "Genus \t Total_species_sequence_download \t Number_of_regulated_species \t Number_of_regulated_species_after_QC \t Number_of_regulated_species_with_oligos\n" > /isilon/biodiversity/users/oligo_database/raw/Oligo_database_species_summary.tab


for taxa in ../raw/*;
        do
        cd /isilon/biodiversity/users/oligo_database/raw/$taxa/ 

	total_species_sequence_download=$(grep ">" Preperations/*NE.QC.fasta | cut -d "|" -f 2 | cut -d "_" -f 2-3 | sort | uniq | wc -l) 
	number_of_regulated_species=$(grep "" species.txt | sort | uniq | wc -l)
	regulated_species_after_QC=$(grep " Y " Statistics/Oligos_results/Species_Oligos_Summary | wc -l)
	regulated_no_oligos=$(grep " Y " Statistics/Oligos_results/Species_Oligos_Summary | grep " 0"| wc -l)
	regulated_with_oligos=$((regulated_species_after_QC-regulated_no_oligos))

echo -e "$taxa \t $total_species_sequence_download \t $number_of_regulated_species \t $regulated_species_after_QC \t $regulated_with_oligos\n" >> ../Oligo_database_species_summary.tab

done

sed -i 's/..\/raw\///g' ../Oligo_database_species_summary.tab

