#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Genus_seq_summary"
#$ -S /bin/bash

/isilon/biodiversity/users/oligo_database/ITS_database_base_scripts/Summary_seq_download.sh /isilon/biodiversity/users/oligo_database/raw/Genus Genus

