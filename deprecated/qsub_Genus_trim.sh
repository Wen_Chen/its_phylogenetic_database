#!/bin/bash
#$ -q all.q
#$ -pe smp 4
#$ -cwd
#$ -N "Genus_trim"
#$ -S /bin/bash

/opt/bio/mothur/mothur "#trim.seqs(fasta=Genus.NE.QC.fasta, minlength=100, processors=4)"
