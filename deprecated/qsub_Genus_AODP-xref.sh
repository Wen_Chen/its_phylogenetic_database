#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "Genus_AODP-xref"
#$ -S /bin/bash

/home/AAFC-AAC/minrul/AODP-execultable/aodp-xref --oligo-fasta-file=Genus.oligo.fasta --oligo-tab-file=Genus.oligo.tab --database= --taxonomy= --node-list=Genus.node-list  --tree-file=Genus.newick --threads=$NSLOTS

