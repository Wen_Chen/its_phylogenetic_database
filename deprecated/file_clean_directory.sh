#!/bin/bash

for f in /isilon/biodiversity/users/oligo_database/raw/*;

{
rm $f/*.NE*
rm $f/error*
rm $f/mothur.*
rm $f/qsub*
rm $f/Sequence_summary
rm $f/*oligo*
rm $f/*_AODP*
rm $f/*_fastTree*
rm $f/*_MAFFT*
rm $f/*.lineage
rm $f/*.newick
rm $f/*.node-list
rm $f/*_QC*
rm $f/*_rename*
rm $f/*_trim*


}
