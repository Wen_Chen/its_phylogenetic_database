#!/bin/bash

folderPath=$1
Genus=$2

Original_sequence=$(grep ">" $folderPath/Preperations/$Genus.fasta | wc -l)

Renaming_error=$(grep "Error:" $folderPath/running_info/$Genus.error.txt | wc -l)

Post_ITSx_sequence=$(grep ">" $folderPath/Preperations/$Genus.NE.QC.fasta | wc -l)

Post_Trim_sequence=$(grep ">" $folderPath/Preperations/$Genus.NE.QC.trim.fasta | wc -l)

Number_of_oligos=$(grep "" $folderPath/AODP_output/Output/*.lenall.superspecific.tab | wc -l)


echo "There are $Original_sequence original sequence." > $folderPath/Statistics/Sequence_summary
echo "There are $Renaming_error renaming errors." >> $folderPath/Statistics/Sequence_summary
echo "There are $Post_ITSx_sequence post ITSx sequence." >> $folderPath/Statistics/Sequence_summary
echo "There are $Post_Trim_sequence post trim sequence.">> $folderPath/Statistics/Sequence_summary
echo "There are $Number_of_oligos oligos.">> $folderPath/Statistics/Sequence_summary

echo "">> $folderPath/Statistics/Sequence_summary

number_of_regulated_species=$(grep "" $folderPath/species.txt | wc -l)
echo "There are $number_of_regulated_species regulated species." >> $folderPath/Statistics/Sequence_summary

while read p;
	do
	search_string=$(echo $p | sed -e 's/\s/_/g')
	Species_sequence=$(grep "$search_string" $folderPath/Preperations/$Genus.NE.QC.fasta | wc -l)
	echo "There are $Species_sequence sequences downloaded for $p.">> $folderPath/Statistics/Sequence_summary
	echo -e "$Genus \t $p \t $Species_sequence" >> $folderPath/../Regulated_Species_info_summary.tab
	if [ "$Species_sequence" -eq 0 ];
	 	then 
		echo -e "$Genus \t $p" >> $folderPath/../Regulated_Species_missing_sequences_summary.tab
	fi
done <$folderPath/species.txt

Input_Line=$(grep "" $folderPath/Statistics/Oligos_results/Species_Oligos_Summary | wc -l)

echo "$Input_Line"

for i in $(eval echo {2..$Input_Line}) ;
        do
        info=$(sed -e "$i q;d" $folderPath/Statistics/Oligos_results/Species_Oligos_Summary)

        echo "$info"
        echo -e "$Genus \t $info \n" >> $folderPath/../Combined_Species_Oligo_Summary_superspecific.tab
done






