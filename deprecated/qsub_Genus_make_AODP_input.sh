#!/bin/bash
#$ -q all.q
#$ -pe smp 4
#$ -cwd
#$ -N "Genus_MakeAODPInput"
#$ -S /bin/bash

mkdir /isilon/biodiversity/users/oligo_database/raw/Genus/AODP_input
cp *.NE.*.fix.fasta /isilon/biodiversity/users/oligo_database/raw/Genus/AODP_input
cp *.FastTree.tre /isilon/biodiversity/users/oligo_database/raw/Genus/AODP_input
touch /isilon/biodiversity/users/oligo_database/raw/Genus/AODP_input/outgroup.outgroup

