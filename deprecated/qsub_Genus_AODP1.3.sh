#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "Genus_AODP" 
#$ -S /bin/bash

/home/AAFC-AAC/minrul/Older_AODP_versions/AODP_v1.3.1/AODP.pl -u g__Genus -c /isilon/biodiversity/users/oligo_database/raw/Genus/config
