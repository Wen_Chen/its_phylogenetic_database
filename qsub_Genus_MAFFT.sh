#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "Genus_MAFFT"
#$ -S /bin/bash

/opt/bio/mafft/bin/mafft --thread $NSLOTS --auto --adjustdirectionaccurately Genus.QC.fasta > Genus.QC.MAFFT-direction.fasta

