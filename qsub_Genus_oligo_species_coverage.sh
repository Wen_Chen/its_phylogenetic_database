#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Genus_oligo_species_coverage"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

perl $script/Oligo_species_coverage.pl Genus.lineage Genus.oligo.tab Genus_regulated_species.txt $raw/download_seq/TaxonID_lineage

