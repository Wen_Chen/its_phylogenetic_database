#!/usr/bin/env perl
use strict;
use warnings;
use Bio::SeqIO;
use Bio::DB::Fasta;


my $indata=shift();
my $outprefix=shift();
my $itsx_path=shift();
my $processors=shift();

my $itsx=`$itsx_path -i $indata -o $outprefix --preserve T --save_regions all --silent T --cpu $processors`;

my $none_found="$outprefix"."_no_detections.fasta";

my $errorDB=Bio::DB::Fasta->new($none_found);
my $input=Bio::SeqIO->new(-file=>"$indata", -format=>'Fasta');
my $output=(split('.fasta',$indata))[0];
$output="$output.QC.fasta";
my $outFasta=Bio::SeqIO->new(-file=>">$output", -format=>'Fasta');

while (my $seq = $input->next_seq()){
    my $id = $seq->id();
    my $dbSeq= $errorDB->header($id);
    if(defined($dbSeq)){
        next();
    }else{
        $outFasta->write_seq($seq);
    }
}
