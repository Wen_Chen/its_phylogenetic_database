#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Oligo_database_update"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

$script/change_paths.sh ${1%/}

qsub $script/qsub_IF.sh

qsub -hold_jid IndexFungorum $script/qsub_Process_IF_outcome.sh

qsub -hold_jid Process_IF_outcome $script/qsub_Generate_mapping_file.sh

qsub -hold_jid Generate_mapping_file $script/qsub_generate_query.sh

qsub -hold_jid Generate_query $script/qsub_download_seq.sh

echo -e `date +%Y%m%d%H%M` >> $oligo/update_date_log;

qsub -hold_jid Download_seq $script/qsub_rename_genbank.sh

qsub -hold_jid Genbank_rename $script/qsub_combine_fasta.sh

qsub -hold_jid Combine_fasta $script/qsub_copy_run_qsubs.sh

while [ ! -f $raw/Genus_done ]
do
	sleep 1m;
done
sleep 5s;

qsub -hold_jid Copy_and_run_Genus_qsubs $script/qsub_combine_summary.sh

qsub -hold_jid Combine_summary $script/qsub_update_database.sh
