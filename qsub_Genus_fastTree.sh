#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Genus_fastTree"
#$ -S /bin/bash

/opt/bio/FastTree/FastTree -gtr -nt Genus.QC.MAFFT-direction.fix.fasta > Genus.QC.MAFFT-direction.FastTree.tre

