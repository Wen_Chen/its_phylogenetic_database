#!/usr/bin/perl
use warnings;
use strict;
use List::MoreUtils qw(uniq);
use List::MoreUtils qw(indexes);
use Data::Dumper qw(Dumper);

my $lineageFile = shift();
my $oligoFile = shift();
my $speciesFile = shift();
my $taxonlineageFile = shift();

my @species_header;
my @Nodes;
my @Species;
my @Nodeslist;
my @oligos;
my @regulated_species;

my %hash_lineage;
my %hash_Node_info;
my %hash_Node_intersect;
my %hash_Species_Complex;

# Open the files
open(my $lf, '<:encoding(UTF-8)', $lineageFile) or die "Could not open lineage.txt file.";
open(my $of, $oligoFile) or die "Could not open oligo's tab file.";
open(my $sf, '<:encoding(UTF-8)', $speciesFile) or die "Could not open species.txt file.";
open(my $tlf, '<:encoding(UTF-8)', $taxonlineageFile) or die "Could not open TaxonID_lineage file.";

# For each row in the taxon lineage file, store into hash with taxonID as key and species as value;
while (my $line = <$tlf>) {
    chomp $line;
    my $taxonid = (split /\t/, $line)[0];
    my $species = (split/s__/, $line)[1];
    $hash_lineage{$taxonid} = $species;
}

#print Dumper \%hash_lineage;

# For each row in the lineage file, cut and determine the Species name (Genus_Species) and Node information (Node#:Node#...), and store them into arrays (Species and Nodes)
while (my $line = <$lf>) {
    chomp $line;
    $line =~ s/['\&]//g;
    my @id = split /:([^:]+)$/, $line;
    push (@Nodeslist, "$id[0]");
    push (@species_header, "$id[1]");

    my @header_n = split /[\:]/, $id[0];
    for my $i (0 .. $#header_n) {
        push (@Nodes, $header_n[$i]);
    }
#    print "@Nodes\n";

    my $taxonid = (split /[\|]/, $id[1])[2];
#    print "taxonid $taxonid]\t";
    my $sci = $hash_lineage{$taxonid};
    $sci =~ s/\s/_/g;
    $sci =~ s/\,/_/g;
    $sci =~ s/\(/_/g;
    $sci =~ s/\)/_/g;
#    print "sci $sci\n";
    push (@Species, $sci);
}

# Save all oligos into an array
while (my $line = <$of>) {
    chomp $line;
    push(@oligos,$line);    
}
#    print "@oligos\n";

# Save all regulated species into an array
while (my $line = <$sf>) {
    chomp $line;
    $line =~ tr/ /_/;
    $line =~ tr/-/_/;
    push(@regulated_species,$line);    
}
#    print "Regulated_species:@regulated_species\n";

# Determine the unique Species names
my @unique_species = uniq sort @Species;

my %hash_species_sequence_number = ();

# For each unique Species name, get all nodes information
foreach my $i (@unique_species) {
#    print "$i\n";    
    # Determine the indexes of matching species name in the Species array
    my @Index_Species_match = indexes {$_ eq $i} @Species;
#    print "Index_Species_match:  @Index_Species_match\n";

    # Output number of sequences for each species
    $hash_species_sequence_number{$i} = $#Index_Species_match +1;

    # Save all matching nodes into temp_array
    my @temp_array = ();
    for my $j (0 .. $#Index_Species_match) {                            
        push (@temp_array, "$Nodeslist[$Index_Species_match[$j]]");
    }
    # Store node informations as hash values
    for my $k (0 .. $#temp_array) {
        $hash_Node_info{$i}[$k]= $temp_array[$k];
    }
}
#    print Dumper \%hash_Node_info;

# For each species stored in hash_Node_info
foreach my $key (keys %hash_Node_info) {
#    print "$key\n";    
    my @Node_array = @{$hash_Node_info{$key}};
#    print "@{$hash_Node_info{$key}}\n";
    my @temp_array = ();
    # If there is only one Node_info
    if ($#Node_array == 0) {
        if (exists $hash_Node_intersect{$Node_array[0]}) {
            $hash_Node_intersect{$Node_array[0]} = "0";
        } else {
            $hash_Node_intersect{$Node_array[0]} = $key;
#		print "NODE: $Node_array[0]\n";
        }        
    } else {
        # If there is more then one Node_info, get intersections between each Node_information (split the Node#, then join the intersections)
        for my $i (0 .. $#Node_array) {
            push (@temp_array, "$Node_array[$i]");
            for my $j (0 .. $#Node_array) {
                if ($i ne $j) {
                    my @temp_i = split /[\:]/, $Node_array[$i];
                    my @temp_j = split /[\:]/, $Node_array[$j];
                    my %original = ();
                    my @isect = ();
                    map { $original{$_} = 1 } @temp_i;
                    @isect = grep { $original{$_} } @temp_j;
                    my @Node_intersections = sort { substr($a, 4) <=> substr($b, 4)  } @isect;

#                    print "Node intersections: @Node_intersections\n";            
                    my $join_nodes = $Node_intersections[0];
                    for my $k (1 .. $#Node_intersections) {
                        $join_nodes = join (":",$join_nodes,$Node_intersections[$k]);
                    }
                    push (@temp_array, "$join_nodes");
    #                print "Join nodes: $join_nodes\n";
                }
            }
        }
        # Duplicate node_intersection is not needed
        my @unique_node_intersection_array = uniq @temp_array;
#        print "unique_node_intersection_array: @unique_node_intersection_array\n";
        # For all Node_intersections, if they haven't exist yet, record them into a new hash as key and species name as values; if they already exist, change the value to 0
        for my $l (0 .. $#unique_node_intersection_array) {
#            print "Node_intersect:  $unique_node_intersection_array[$l]\n";
            if (exists $hash_Node_intersect{$unique_node_intersection_array[$l]}) {
                $hash_Node_intersect{$unique_node_intersection_array[$l]} = "0";
            } else {
                $hash_Node_intersect{$unique_node_intersection_array[$l]} = $key;
            }
#            print "hash:  $hash_Node_intersect{$unique_node_intersection_array[$l]}\n"
        }
    }
}

#    print Dumper \%hash_Node_intersect;


# Search to make sure the Nodes are actually species specific, and remove those nodes that are not possible to be species complex
foreach my $key (keys %hash_Node_intersect) {
    # Search Indexes with Nodes information with matching substrings as Nodes for Species complex
    if ($hash_Node_intersect{$key} eq "0") {
        delete $hash_Node_intersect{$key};
    } elsif ($hash_Node_intersect{$key} ne "0") {
	my @Index_Node_search = indexes {$_ =~ $key} @Nodeslist;
        for my $i (0 .. $#Index_Node_search) {
	    print "$Species[$Index_Node_search[$i]]\t";
	    print "$hash_Node_intersect{$key}\n";
	    if ($Species[$Index_Node_search[$i]] ne $hash_Node_intersect{$key}) {
		delete $hash_Node_intersect{$key};
		last;
	    }
	}
    }
}

#    print Dumper \%hash_Node_intersect;

# Output Species complex information

my @node_specific_species = ();
my @node_specific_nodes = ();

# Open output file for output
my $lin_output = "Node_for_specific_species_Summary";
open(LIN_OUTFILE, '>', $lin_output);

foreach my $key (sort keys %hash_Node_intersect) {
    my $Species_name = $hash_Node_intersect{$key};
    push (@node_specific_species, $Species_name);
    my @Node_numbers = split /:([^:]+)$/, $key;
    push (@node_specific_nodes, $Node_numbers[1]);
    my @Node_sequence = indexes {$_ =~ $key} @Nodeslist;
    my $Node_sequence_number = $#Node_sequence +1;
    print (LIN_OUTFILE "At $Node_numbers[1], there are only $Node_sequence_number sequences for $Species_name.\n");
}

#    print "node_specific_species: @node_specific_species\n";
#    print "node_specific_nodes: @node_specific_nodes\n";

close(LIN_OUTFILE);

my @unique_Nodes = uniq @Nodes;
#    print "unique_Nodes:  @unique_Nodes\n";

my %hash_oligos_matching = ();

# Output a summary for the total number of oligos tied to each leaf/node
my $oli_summary_output = "Oligos_Summary";
open(OLI_SUMMARY_OUTFILE, '>', $oli_summary_output);
print (OLI_SUMMARY_OUTFILE "LEAF/NODE \t Number of Oligos\n");    

# Calculate total number of oligos from the oligoFile
my $totalOligos =`wc -l < $oligoFile`;
#    print "Total_oligos: $totalOligos";
print (OLI_SUMMARY_OUTFILE "Total \t $totalOligos");    

# For each species_header (full species name information)
for my $i (@species_header) {
    my @matching_unique_info = split /[\|]/, $i;
#    print "@matching_unique_info\n";
    # Search if they appear in the oligos file
    my @match_species_header = indexes {$_ =~ $matching_unique_info[0]} @oligos;
#   print "$i \t $#match_species_header\n\n";
        
    # print the species name and total number of oligos, and record matching oligos them into hash_oligos_matching and also print to leaf_oligo file
    if ($#match_species_header > -1) {
        my $output_number = $#match_species_header+1 ;
        print (OLI_SUMMARY_OUTFILE "$i \t $output_number\n");
        for my $j (0 .. $#match_species_header) {
            $hash_oligos_matching{$i}[$j] = $oligos[$match_species_header[$j]];
        }
    } else {
        print (OLI_SUMMARY_OUTFILE "$i \t 0\n");
        $hash_oligos_matching{$i} = "None";
    }
}

#   print Dumper \%hash_oligos_matching;

# For each Node#
for my $i (@unique_Nodes) {
    # Search if they appear in the oligos file
    my @match_unique_Nodes = indexes {$_ =~ "$i-"} @oligos;
#    print "$i \t $#match_unique_Nodes\n";

    # If found, print the Node$ and total number of oligos, and record them into hash_oligos_matching
    if ($#match_unique_Nodes > -1) {
        my $output_number = $#match_unique_Nodes+1 ;
        print (OLI_SUMMARY_OUTFILE "$i \t $output_number\n");
        for my $j (0 .. $#match_unique_Nodes) {
            $hash_oligos_matching{$i}[$j] = $oligos[$match_unique_Nodes[$j]];
        }
    } else {
        print (OLI_SUMMARY_OUTFILE "$i \t 0\n");
        $hash_oligos_matching{$i} = "None";    
    }
}

#    print Dumper \%hash_oligos_matching;

close(OLI_SUMMARY_OUTFILE);

my %hash_oligos_count_for_species = ();

# For each unique species name, generate the species_oligo file, retrieve all oligos for each leaf for the species and nodes that are specific for only this species
for my $i (@unique_species) {
    print "species_name: $i\n";
    my $species_oligos_output = join ("_",$i,"Oligos");
#    print "Output file name: $species_oligos_output\n";
    open(OLI_SPECIES_OUTFILE, '>', $species_oligos_output);

    my @species_leaf_index_match = indexes {$_ eq $i} @Species;
#    print "species_leaf_index_match: @species_leaf_index_match\n";

    for my $j (0 .. $#species_leaf_index_match) {
#        print ("$j\n");    
#        print ("leaf name:  $species_header[$species_leaf_index_match[$j]]\n");
#        print ("Values:  $hash_oligos_matching{$species_header[$species_leaf_index_match[$j]]} \n");
        if ($hash_oligos_matching{$species_header[$species_leaf_index_match[$j]]} ne "None") {
            foreach my $oligo_values (@{$hash_oligos_matching{$species_header[$species_leaf_index_match[$j]]}}) {;
#               print ("oligos:$oligo_values\n");
                print (OLI_SPECIES_OUTFILE "$oligo_values \n");    
            }
        }
    }

    my @species_node_specific_index_match = indexes {$_ =~ $i} @node_specific_species;
#    print ("$#species_node_specific_index_match \n");

    if ($#species_node_specific_index_match > -1) {
        for my $k (0 .. $#species_node_specific_index_match) {
#            print ("$k\n");    
#            print ("Node#:  $node_specific_nodes[$species_node_specific_index_match[$k]]\n");
#            print ("Values:  $hash_oligos_matching{$node_specific_nodes[$species_node_specific_index_match[$k]]} \n");

            if ($hash_oligos_matching{$node_specific_nodes[$species_node_specific_index_match[$k]]} ne "None") {
                foreach my $oligo_values (@{$hash_oligos_matching{$node_specific_nodes[$species_node_specific_index_match[$k]]}}) {;
#                    print ("oligos:$oligo_values\n");
                    print (OLI_SPECIES_OUTFILE "$oligo_values \n");    
                }
            }
        }
    }

    my $oligo_count = `wc -l < $species_oligos_output`;
    print "oligo count = $oligo_count";

    close(OLI_SPECIES_OUTFILE);

#    print "Oligo_count: $oligo_count\n";
    $hash_oligos_count_for_species{$i} = $oligo_count;
}

#    print Dumper \%hash_oligos_count_for_species;

# Open output file for output
my $species_summary_output = "Species_Oligos_Summary";
open(SPECIES_SUMMARY_OUTFILE, '>', $species_summary_output);
print (SPECIES_SUMMARY_OUTFILE "Species_name \t Regulated_or_not \t Number_of_sequeces_downloaded \t Number_of_oligos\n");
 
for my $i (@unique_species) {
#    print "species_name: $i\n";
    my $R;
    if (grep(/^$i$/, @regulated_species)) {
        $R = "Y";
    } else {
	$R = "N";
    }
    print (SPECIES_SUMMARY_OUTFILE "$i\t$R\t$hash_species_sequence_number{$i}\t$hash_oligos_count_for_species{$i}");

}

close(SPECIES_SUMMARY_OUTFILE);

