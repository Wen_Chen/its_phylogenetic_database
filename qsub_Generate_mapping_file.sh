#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "Generate_mapping_file"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";
tmp="$oligo/tmp";

if [ ! -d "$tmp" ]; then
        mkdir $tmp;
fi

perl $script/Generate_mapping_file.pl $raw $tmp

