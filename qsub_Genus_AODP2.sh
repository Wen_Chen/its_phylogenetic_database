#!/bin/bash
#$ -q all.q
#$ -pe smp 4
#$ -cwd
#$ -N "Genus_AODP"
#$ -S /bin/bash

/isilon/biodiversity/users/chenw_lab/Manuscripts/AODP3_testing/AODP2.4.1.1/aodp --basename=Genus --tree-file=Genus.QC.MAFFT-direction.FastTree.tre --oligo-size=15-30 Genus.QC.MAFFT-direction.fix.fasta

