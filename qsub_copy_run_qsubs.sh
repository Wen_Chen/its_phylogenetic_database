#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Copy_and_run_Genus_qsubs"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";
current=$(pwd);

cat $raw/Original_genus_list | while read genus;
        do
                cp $script/qsub_Genus* $raw/$genus;
                rename Genus $genus $raw/$genus/qsub*;
                sed -i -e "s/Genus/$genus/g" $raw/$genus/qsub*
		cd $raw/$genus/;
		./qsub_"$genus"_scripts.sh;
done;

cd $current;

touch $raw/Genus_progress

Ori=$(wc -l $raw/Original_genus_list | awk '{print $1}');
echo "$Ori";
progress="0";

while [ "$Ori" -ne "$progress" ];
	do
		sleep 5m;
		progress=$(wc -l $raw/Genus_progress | awk '{print $1}');
echo "$progress";
done;

mv $raw/Genus_progress $raw/Genus_done;
