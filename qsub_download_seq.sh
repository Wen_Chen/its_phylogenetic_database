#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "Download_seq"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

perl $script/downloadSeqsFromGenBank.pl -query $raw/download_seq/genus.query -out $raw/download_seq/

sleep 15s
