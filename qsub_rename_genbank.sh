#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "Genbank_rename"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";
current=$(pwd);

cd $raw/download_seq/

for i in *.fasta;
	do
	Ori=$(grep ">" $i | wc -l);
        ne=${i//.fasta/".NE.fasta"};

	NE="0";

	# check for sequence number match due to possible collision with NCBI
	while [ "$Ori" -ne "$NE" ]; 
		do 
		$script/rename_genbank_sequences.pl $i $raw/download_seq/TaxonID_lineage
		sleep 15s

		NE=$(grep ">" $ne | wc -l);
	done;

done;

cd $current
