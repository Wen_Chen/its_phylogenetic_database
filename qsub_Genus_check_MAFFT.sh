#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Genus_MAFFT_fix"
#$ -S /bin/bash

PATH_TO_DATABASE/its_phylogenetic_database/fix_MAFFT_RC.sh Genus.QC.MAFFT-direction.fasta

