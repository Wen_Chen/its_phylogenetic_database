#!/bin/bash

old='PATH_TO_DATABASE';
new=${1%/};

for file in $new/its_phylogenetic_database/qsub_*.sh; do
	sed -i "s%$old%$new%g" $file;
done

sed -i "s%$old%$new%g" $new/its_phylogenetic_database/Oligo_database_update.sh;

for file in $new/its_phylogenetic_database/mix_*.sh; do
        sed -i "s%$old%$new%g" $file;
done

