#!/usr/bin/perl
use warnings;
use strict;
use List::MoreUtils qw(uniq);
use List::MoreUtils qw(indexes);
use Data::Dumper qw(Dumper);
use Bio::Taxon;
use Bio::DB::Taxonomy;

my $path = shift;
my $tmp = shift;

my $IFFile = "$path/Synonym_and_current_name_list_from_IF";

my $taxon_db = Bio::DB::Taxonomy->new(-source => 'flatfile',
			-nodesfile => '/isilon/biodiversity/reference/ncbi_taxonomy/nodes.dmp',
			-namesfile => '/isilon/biodiversity/reference/ncbi_taxonomy/names.dmp',
			-directory => $tmp);
my $nameFile = '/isilon/biodiversity/reference/ncbi_taxonomy/names.dmp';
my $nodeFile = '/isilon/biodiversity/reference/ncbi_taxonomy/nodes.dmp';

# Open the synonym file as well as the dump files
open(my $sf, '<', $IFFile) or die "Could not open $IFFile";
open(my $nof, '<', $nodeFile) or die "Could not open NCBI taxonomy node list $nodeFile";
open(my $naf, '<', $nameFile) or die "Could not open NCBI taxonomy name list $nameFile";

my @ori_sp;
my @ori_G_list;
my @que_G_list;
my %hash_species;
my %hash_Genus;
my %hash_node_ancestor;
my %hash_node_rank;
my %hash_name_sci;
my @nofound_IF_NCBI;
my @found_only_NCBI;
my %hash_NCBI_record;

# For each row in the node dump taxonomy file, store ancestor ID and rank into hash with TaxonID as key
while (my $line = <$nof>) {
	chomp $line;
	my ($ID, $parent_ID, $Rank) = (split(/\t/, $line))[0,2,4];
	$hash_node_ancestor{$ID} = $parent_ID;
	$hash_node_rank{$ID} = $Rank;
}

#print Dumper \%hash_node_ancestor;
#print Dumper \%hash_node_rank;
$| = 1;
print "Node dump, done\n";

# For each row in the name dump taxonomy file, store the scientific name into hash with TaxonID as key
while (my $line = <$naf>) {
        chomp $line;
        my ($ID, $name, $type) = (split(/\t/, $line))[0,2,6];
	
	if ($type =~ "scientific name") {
		$hash_name_sci{$ID} = $name;
        }
}

#print Dumper \%hash_name_sci;
$| = 1;
print "Name dump, done\n";

# Open output file for output IF_NCBI_Taxonomy_Summary.txt
my $map_output = "$path/IF_NCBI_Taxonomy_Summary.txt";
open(MAP_OUTFILE, '>', $map_output);
print (MAP_OUTFILE "Original_name\tGenus_of_Original_name\tOri_TaxonID\tOri_Genus_from_TaxonID\tOri_Current_name_in_IF\tSynonym_name_from_IF\tSyn_TaxonID\tSyn_Genus_from_TaxonID\tNCBI_scientific_name\tIF_record\tNCBI_record\tIF_currentname_vs_NCBI_scientificname_match\n");

# For each row in the synonym file, 
while (my $line = <$sf>) {
	next if $. == 1;
	chomp $line;
	$| = 1;
	print "$line\n";

	my $reg_ID;
	my $reg_G = "-";
	my $syn_ID;
	my $syn_G = "-";
	my $sci = "-";
	my $IF = "Y";		# default, exist in IF
	my $NCBI = "N"; 	# default, doesn't exist in NCBIi
	my $match = "N";	# default, IF current name doesn't match scientific name from NCBI

        my @species= split "\t", $line;
        if ($species[1] eq "Null") {
                $species[1] = $species[0];
		$IF = "N";      # Change to "N" as Null indicates species from nofound list
        }
        if ($species[2] eq "Null") {
                $species[2] = "-";
        }
	push (@ori_sp,$species[0]);
	
	my $ori_G = (split/\s/,$species[0])[0];
	push (@ori_G_list, $ori_G);
	push (@que_G_list, $ori_G);

	eval {
		$reg_ID = $taxon_db->get_taxonids("$species[0]");
		$syn_ID = $taxon_db->get_taxonids("$species[1]");
	};

        # if the regulated species have taxonID, get the Genus and push to an array, else if the regulated species doesn't have taxonID, aka doesn't exist in NCBI, push the species name into another array
	if (defined $reg_ID) {
# && (getKingdom($reg_ID) eq "Fungi" || getKingdom($reg_ID) eq "Oomycetes")) {
		$reg_G = getGenus($reg_ID);
		
#		print "genus $reg_G\n";
		push (@que_G_list, $reg_G);
	} else {
                $reg_ID = "-";
	}

        # if the synonym species have taxonID, get the Genus and push to an array
        if (defined $syn_ID) {
# && (getKingdom($syn_ID) eq "Fungi" || getKingdom($syn_ID) eq "Oomycetes")) {
		$sci = "$hash_name_sci{$syn_ID}";
		if ($sci eq $species[2]) {
			$match = "Y";
		}
#		print "sci $sci\n";
		
		$syn_G = getGenus($syn_ID);
#		print "genus $reg_G\n";
		push (@que_G_list, $syn_G);
		$NCBI = "Y" 	# Change to "Y" as synonym have TaxonID, aka exist in NCBI
	} else {
                $syn_ID = "-";
	}

	# print the information for the taxonomy mapping file
	print (MAP_OUTFILE "$species[0]\t$ori_G\t$reg_ID\t$reg_G\t$species[2]\t");
	print (MAP_OUTFILE "$species[1]\t$syn_ID\t$syn_G\t$sci\t");
	print (MAP_OUTFILE "$IF\t$NCBI\t$match\n");

	if ($IF eq "N" && $NCBI eq "N") {
		push (@nofound_IF_NCBI, $species[1]);
	} elsif ($IF eq "N" && $NCBI eq "Y") {
		push (@found_only_NCBI, $species[1]);
	}

	unless (exists $hash_NCBI_record{$species[0]} ) {
		$hash_NCBI_record{$species[0]} = $NCBI;
	} elsif ($hash_NCBI_record{$species[0]} ne "Y") {
		$hash_NCBI_record{$species[0]} = $NCBI;
	}

	# if in %hash_Genus there exists a key with the original genus, add the regulated and synonym genus to the hash array; else, the zero index of hash array will be the original genus, followed by regulated genus and synonym genus in the 1st and 2nd index
	if (exists $hash_Genus{$ori_G}) {
		$hash_Genus{$ori_G}[@{$hash_Genus{$ori_G}}] = $reg_G;
		$hash_Genus{$ori_G}[@{$hash_Genus{$ori_G}}] = $syn_G;
	} else {		
			$hash_Genus{$ori_G}[0] = $ori_G;
			$hash_Genus{$ori_G}[1] = $reg_G;
			$hash_Genus{$ori_G}[2] = $syn_G;
	}
	# if in %hash_species there exists a key with the regulated genus, add the regulated species and synonym species to the hash array; else, the zero index of hash array with the regulated genus will be the regulated species, followed by synonym species in the 1st index
	if (exists $hash_species{$ori_G}) {
		$hash_species{$ori_G}[@{$hash_species{$ori_G}}] = $species[0];
		$hash_species{$ori_G}[@{$hash_species{$ori_G}}] = $species[1];
	} else {		
		$hash_species{$ori_G}[0] = $species[0];			
		$hash_species{$ori_G}[1] = $species[1];
	}
}

#print Dumper \%hash_Genus;
#print Dumper \%hash_species;

close (MAP_OUTFILE);

#Open output file for output a list containing the all original genus name
my $G_list_output = "$path/Original_genus_list";
open(G_list_OUTFILE, '>', $G_list_output);
my @uniq_ori_G_list = @ori_G_list;
@uniq_ori_G_list = map { remove_accent($_) } @uniq_ori_G_list;
@uniq_ori_G_list = uniq @uniq_ori_G_list;
# Output a list of unique regulated Genus
foreach my $i (sort @uniq_ori_G_list) {
	print (G_list_OUTFILE "$i\n");
}
close (G_list_OUTFILE);

#Open output file for output a list of genus for query to download sequences
my $q_list_output = "$path/Query_genus_list";
open(Q_list_OUTFILE, '>', $q_list_output);
my @uniq_que_G_list = uniq @que_G_list;
# Output a list of unique regulated Genus
foreach my $i (sort @uniq_que_G_list) {
	print (Q_list_OUTFILE "$i\n");
}
close (Q_list_OUTFILE);

# Open output file for output Genus_genera and Genus_regulated_speces.txt based on the original genus name which is used as the keys in %hash_Genus and %hash_species
foreach my $key (sort keys %hash_Genus) {
	my $no_acc_key = `echo $key | iconv -f utf-8 -t ascii//translit`;
	chomp $no_acc_key;

	if ($no_acc_key ne $key) {
		my $G_gen_output = join("_", "$path/$no_acc_key", "genera");
	        open(G_gen_OUTFILE, '>>', $G_gen_output);
		my $G_sp_output = join("_", "$path/$no_acc_key", "regulated_species.txt");
	        open(G_sp_OUTFILE, '>>', $G_sp_output);
	} else {
		my $G_gen_output = join("_", "$path/$key", "genera");
	        open(G_gen_OUTFILE, '>', $G_gen_output);
		my $G_sp_output = join("_", "$path/$key", "regulated_species.txt");
	        open(G_sp_OUTFILE, '>', $G_sp_output);
	}

	# Retrieve all genus stored with original Genus as key in %hash_Genus
	my @Genus;
	for my $i (0 .. @{$hash_Genus{$key}}-1) {
		push (@Genus, $hash_Genus{$key}[$i]);
	}
	
	my @uniq_Genus = uniq (sort @Genus);

	#if "-" was added to Genus due no TaxonID, remove it from the array
	if (grep {$_ eq "-"} @uniq_Genus) {
		splice @uniq_Genus,0,1;
	}

	for my $j (0 .. $#uniq_Genus) {	
		print (G_gen_OUTFILE "$uniq_Genus[$j]\n");
	}
	close(G_gen_OUTFILE);

	# Retrieve all regulated and synonym species name stored with original Genus as key in %hash_species
	my @reg_species;
	for my $i (0 .. @{$hash_species{$key}}-1) {
		push (@reg_species, $hash_species{$key}[$i]);
	}
	my @uniq_species = uniq (sort @reg_species);
	for my $j (0 .. $#uniq_species) {
		print (G_sp_OUTFILE "$uniq_species[$j]\n");
	}
	close(G_sp_OUTFILE);

	if ($no_acc_key ne $key) {
		delete $hash_Genus{$key};
	}
}

# Generated summary file for the IF_NCBI_Taxonomy_Summary.txt file
my $summary_output = "$path/Summary_for_IF_NCBI_Taxonomy_Summary";
open(S_OUTFILE, '>', $summary_output);

my @uniq_sp = uniq sort @ori_sp;
print (S_OUTFILE "Total # of regulate species in the original given list\t", scalar @uniq_sp,"\n");

print (S_OUTFILE "Total # of genus from the original given list\t$#uniq_ori_G_list\n");

my $sp_count = `wc -l < $map_output`-1;
print (S_OUTFILE"Total # of synonyms\t$sp_count\n");

my $NCBI_count;
for my $key (sort keys %hash_NCBI_record) {
        if ($hash_NCBI_record{$key} eq "N" ) {
                $NCBI_count ++;
        }
}
print (S_OUTFILE "# of original regulated species with no record as well as synonym records in NCBI\t$NCBI_count\n");

my @uniq_nofound = uniq @nofound_IF_NCBI;
print (S_OUTFILE "# of species with absolutely no record in both IF and NCBI\t", scalar @uniq_nofound, "\n");
print (S_OUTFILE "# of species only found in NCBI\t", scalar @found_only_NCBI,"\n");

close(S_OUTFILE);

############################################################################################################################################# SUB-FUNCTIONS:  getGenus ###########################################################################################################################################

sub getGenus {        # Get Genus name for a given TaxonID using the hashes created from nodes.dmp and names.dmp
	my $taxonID = shift();
	my $Parent_ID;
	my $rank = "";

	while ($rank ne "genus") {
		$Parent_ID = $hash_node_ancestor{$taxonID};
		$rank = "$hash_node_rank{$Parent_ID}";

		if ($rank eq "genus") {
			return $hash_name_sci{$Parent_ID};
		} elsif ($rank ne "genus") {
			$taxonID = $Parent_ID;
		}
	}	
}

sub remove_accent {
	my $word = shift();
	$word = `echo $word | iconv -f utf-8 -t ascii//translit`;
	chomp $word;
	return $word;
}
