#!/bin/bash

path=${1%/};
cwd=$(pwd);

cd $path/download_seq/

for i in *.fasta;
        do
        Ori=$(grep ">" $i | wc -l);
        ne=${i//.fasta/".NE.fasta"};

        NE="0";

        # check for sequence number match due to possible collision with NCBI
        while [ "$Ori" -ne "$NE" ];
                do
                $path/rename_genbank_sequences.pl $i $path/download_seq/TaxonID_lineage
                sleep 15s

                NE=$(grep ">" $ne | wc -l);
        done;

done;

cd $current
