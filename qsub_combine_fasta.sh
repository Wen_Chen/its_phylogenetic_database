#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "Combine_fasta"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

cat $raw/Original_genus_list | while read LINE;
	do 
		mkdir $raw/$LINE;
		mv $raw/"$LINE"_genera $raw/$LINE;
		mv $raw/"$LINE"_regulated_species.txt $raw/$LINE;
		cat $raw/$LINE/"$LINE"_genera | while read genus;
			do
			cat $raw/download_seq/"$genus".NE.fasta >> $raw/$LINE/"$LINE".fasta;
		done;
		# remove duplicate sequences with same seqID
		perl $script/remove_duplicates_ID.pl $raw/$LINE/"$LINE".fasta $raw/$LINE/"$LINE".uniq.fasta;
		mv $raw/$LINE/"$LINE".uniq.fasta $raw/$LINE/"$LINE".fasta;
done;

