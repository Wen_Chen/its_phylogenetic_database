#!/usr/bin/perl
use warnings;
use strict;
use List::MoreUtils qw(uniq);
use List::MoreUtils qw(indexes);
use Data::Dumper qw(Dumper);
use Bio::SeqIO;

my $input = shift();
my $output = shift();

my %hash_seq;

my $in = Bio::SeqIO->new(-file=>"$input", -format=>"fasta");

while (my $sequence = $in->next_seq()) {
	my $id = $sequence->id();
	my $seq = $sequence->seq();

	unless (exists $hash_seq{$id}) {
		$hash_seq{$id} = $seq;
	}
}

open (OUT,'>', $output);

foreach my $key ( keys %hash_seq) {
	print (OUT ">",$key, "\n", $hash_seq{$key}, "\n");
}



