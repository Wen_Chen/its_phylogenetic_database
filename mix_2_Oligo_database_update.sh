#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Oligo_database_update_part_2"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

echo -e `date +%Y%m%d%H%M` >> $oligo/update_date_log;

qsub $script/qsub_combine_fasta.sh

qsub -hold_jid Combine_fasta $script/qsub_copy_run_qsubs.sh

while [ ! -f $raw/Genus_done ]
do
	sleep 1m;
done
sleep 5s;

qsub -hold_jid Copy_and_run_Genus_qsubs $script/qsub_combine_summary.sh

qsub -hold_jid Combine_summary $script/qsub_update_database.sh
