#!/bin/bash

infile=$1

base=$(echo $infile | cut -d"." -f1-3)
cat $infile | perl -pe 's/(>)(_R_)(.*)/$1$3$2/g' > $base".fix.fasta"
