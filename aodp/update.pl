#!/usr/bin/perl
use strict;
use warnings;
use Cwd;
use File::Copy;

my $htmlFolder = "/var/www/html/aodp";
my $databaseFolder = "$htmlFolder/Processed";
my $remoteDatabasePath = shift;

`rm -rf $databaseFolder`;

`scp -r zhuk\@biocluster.agr.gc.ca:$remoteDatabasePath/Processed $htmlFolder`;
`scp -r zhuk\@biocluster.agr.gc.ca:$remoteDatabasePath/update_date_log $htmlFolder`;

chdir $htmlFolder;

#copy scripts to database folder
copy("regulatedSpecies.pl", $databaseFolder);
copy("Xpaths.pl", $databaseFolder);
copy("fungalPathogensInformation.csv", $databaseFolder);

chdir $databaseFolder;
`perl regulatedSpecies.pl fungalPathogensInformation.csv`;
`perl Xpaths.pl regulatedSpeciesTableInformation.csv`;

copy("$databaseFolder/aodpFilePaths.xml", $htmlFolder);
copy("$databaseFolder/Oligo_database_summary.csv", $htmlFolder);
copy("$databaseFolder/update_date_log", $htmlFolder);

chdir $htmlFolder;
`php createGenusTemplates.php`;
