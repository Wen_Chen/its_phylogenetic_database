#!/usr/bin/perl
use strict;
use warnings;
use Cwd;

my $homedir = getcwd();

my $file = shift;

opendir (my $dh, $homedir) || die "Can't open $homedir: $!";

open (INPUT, "<$file") or die "$file, $!";
open (OUTPUT, ">regulatedSpeciesTableInformation.csv");

#my %data;
my %regulatedSpecie;
my @organizations = ('Canada-CFIA', 'US APHIS', 'US-selected agent', 'EPPO', 'China', 'Japan', 'Mexico', 'India', 'Korea', 'Indonesia', 'Algeria', 'Colombia', 'Argentina', 'Brazil', 'Chili', 'Peru', 'Malaysia');

my $headers = <INPUT>;
chomp $headers;
my @headerNames = split(/,/, $headers);

while (my $line = <INPUT>) {
	chomp $line;
	my @array = split(/,/, $line);
	
	for (my $i=0; $i<scalar(@headerNames); $i++){
		$regulatedSpecie{$headerNames[$i]} = $array[$i];
	}
	
	if ($regulatedSpecie{'Genus'} eq '') {last;}
	
	print OUTPUT "$regulatedSpecie{'Genus'}";
	print OUTPUT ",$regulatedSpecie{'Final Genus Names'}";
	print OUTPUT ",$regulatedSpecie{'Species name (from original list)'}";
	print OUTPUT ",$regulatedSpecie{'Final name'}";
	print OUTPUT ",$regulatedSpecie{'Common name or disease'},";
	
	foreach my $organization (@organizations) {
		if ($regulatedSpecie{$organization}) {
			print OUTPUT "$organization\t";
		}
	}
	
	
	
	print OUTPUT "\n";
}

close INPUT;
close OUTPUT;