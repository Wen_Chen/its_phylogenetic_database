<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

$xmlFile = "aodpFilePaths.xml";
	
$xmlDoc = new DOMDocument();
$xmlDoc->load($xmlFile);

$root = $xmlDoc->documentElement;

$genusArray = [];
if ($root->hasChildNodes()) {
	foreach ($root->getElementsByTagName('genusFolder') AS $genus) {
		$genusArray[] = $genus->getAttribute('name');
	}
}
asort($genusArray);

if (!(file_exists("genus"))) {
	mkdir("genus", 0777);
}

if ($root->hasChildNodes()) {
	foreach ($root->getElementsByTagName('genusFolder') AS $genus) {
		$genusName = $genus->getAttribute('name');
		$templateName = "genus/".strtolower($genusName).".html";
		
		$myTemplate = fopen($templateName, "w") or die ("Unable to open ".$templateName."!");
		fwrite($myTemplate, '
<!DOCTYPE html>

<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");
?>

<html>
	<head>
		<title>AAFC </title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<link href="../css/table/jq.css" rel="stylesheet" type="text/css">
		<link href="../css/table/style.css" rel="stylesheet" type="text/css">
		<link href="../css/style.css" rel="stylesheet" type="text/css">
		<link href="../css/list.css" rel="stylesheet" type="text/css">
		
		<link href="../font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script>
		$(function() {
			var availableGenus = [');
		
		foreach ($genusArray AS $item) {
			fwrite($myTemplate, '"'.$item.'",');
		}
		
		fwrite($myTemplate, '];
			$( "#genus" ).autocomplete({
				source: availableGenus,
				minLength: 1,
				select: function (e, ui) {
					window.location.href = "../genus/" + (ui.item.value).toLowerCase() + ".html";
				}
			});
		});
		</script>
		
		<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script type="text/javascript" src="../includes/tableJS/jquery-latest.js"></script>
		<script type="text/javascript" src="../includes/tableJS/tablesorter.js"></script>
		<script type="text/javascript" src="../includes/tableJS/tablesorter.pager.js"></script>
		<script type="text/javascript">
		$(function() {
			$("table#regulatedSpeciesTable")
				.tablesorter({widthFixed: true})
				.tablesorterPager({container: $("#pager")});
		});
		</script>-->
		
		<script type="text/javascript">
		function fnExcelReport() {
			var tab_text = \'<html xmlns:x="urn:schemas-microsoft-com:office:excel">\';
			tab_text = tab_text + "<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>";
			
			tab_text = tab_text + "<x:Name>Test Sheet</x:Name>";
			
			tab_text = tab_text + "<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>";
			tab_text = tab_text + "</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>";
			
			tab_text = tab_text + \'<table border="1px">\';
			tab_text = tab_text + $("#regulatedSpeciesTable").html();
			//console.log(JSON.stringify(document.getElementById("regulatedSpeciesTable").textContent));
			tab_text = tab_text + "</table></body></html>";
			
			var data_type = "data:application/vnd.ms-excel";
			
			var ua = window.navigator.userAgent;
			var msie = ua.indexOf("MSIE ");
			
			if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
				if (window.navigator.msSaveBlob) {
					var blob = new Blob([tab_text], {
						type: "application/csv;charset=utf-8;"
					});
					navigator.msSaveBlob(blob, "'.strtolower($genusName).'_regulated_species.xls");
				}
			} else {
				$("#exportButton").attr("href", data_type + ", " + encodeURIComponent(tab_text));
				$("#exportButton").attr("download", "'.strtolower($genusName).'_regulated_species.xls");
			}
		}
		</script>
	</head>
	
	<body>
		<div id="wrapper">
			<div id="title">
				<img src="../images/index.jpg" alt="AAFC" title="AAFC"/>
      </div>
			
			<ul id="menu-bar">
				<li><a href="/aodp/">Home</a></li>
				<li><a href="/aodp/moreinfo.html">More Information</a></li>
				<li><a href="/aodp/contact.html">Contact Us</a></li>
				<!--<input id="genus" type="text" class="fontAwesome" name="search" placeholder="&#xf002; Enter a genus name...">-->
			</ul>
			
			<div class="main">
				<div class="autocomplete_container">
					<input id="genus" type="text" class="fontAwesome" name="search" placeholder="&#xf002; Enter a genus name...">
				</div>
				
				<h1><b>'.$genusName.'</b></h1>
				
				<table id="export">
					<tr>
						<td id="input">
							<h2>Reference Files</h2>
							<table id="reference">
								<thead>
									<tr>
										<th>File Name</th>
										<th>File Size</th>
										<th>View</th>
										<th>Download</th>
									</tr>
								</thead>
								
								<tbody>');
		
		#Reference
		foreach ($genus->getElementsByTagName('inputFolder') AS $folder) {
			$files = $folder->getElementsByTagName('file');
			
			foreach ($files AS $file) {
				$myFileName = $file->getAttribute('fileName');
				$myPathToFile = $genusName.'/'.$folder->getAttribute('name').'/'.$myFileName;
				$myFileSize = $file->getAttribute('fileSize');
				
				$disabled = "";
				if ($myFileSize == 0) {
					$disabled = " disabled";
				}
				
				fwrite($myTemplate, '
									<tr>
										<td>'.$myFileName.'</td>
										<td>'.readableFileSize($myFileSize).'</td>
										<td>
											<!--<a href="/aodp/Processed/'.$myPathToFile.'">
												<button type="submit">View</button>
											</a>-->
											<button onclick="viewConfirmation(\'/aodp/Processed/'.$myPathToFile.'\')"'.$disabled.'>View</button>
										</td>
										<td>
											<a href="/aodp/Processed/'.$myPathToFile.'" download="'.$myFileName.'">
												<button type="submit"'.$disabled.'>Download</button>
											</a>
										</td>
									</tr>');
			}
			
			if (sizeof($files) < 3) {
				fwrite($myTemplate, '
									<tr>
										<td>Outgroup File</td>
										<td>N/A</td>
										<td>N/A</td>
										<td>N/A</td>
									</tr>');
			}
		}
		
		fwrite($myTemplate, '	
								</tbody>
							</table>');
						
		#stats
		fwrite($myTemplate, '<dl id="stats">');
		foreach ($genus->getElementsByTagName('stat') AS $stat) {
			$stats = explode("\t", $stat->nodeValue);
			$name = $stats[0];
			$value = $stats[1];
			
			fwrite($myTemplate, '
									<dt class="genusStat">'.$name.'</dt>
									<dd class="genusStat">'.$value.'</dd>');
		}
		fwrite($myTemplate, '</dl></td>');
						
		fwrite($myTemplate, '
						<td id="output">
							<h2>Output Files</h2>
							<table id="output">
								<thead>
									<tr>
										<th>File Name</th>
										<th>File Size</th>
										<th>View</th>
										<th>Download</th>
									</tr>
								</thead>
								
								<tbody>');
		
		#Output
		foreach ($genus->getElementsByTagName('outputFolder') AS $folder) {
			foreach ($folder->getElementsByTagName('file') AS $file) {
				$myFileName = $file->getAttribute('fileName');
				$myPathToFile = $genusName.'/'.$folder->getAttribute('name').'/'.$myFileName;
				$myFileSize = $file->getAttribute('fileSize');
				
				$extension = pathinfo($myFileName, PATHINFO_EXTENSION);
				
				$disableDownload = "";
				$disableView = "";
				if (($myFileSize == 0) || ($extension == "eps")){
					$disableDownload = " disabled";
					$disableView = " disabled";
					continue;
				}
				
				fwrite($myTemplate, '
									<tr>
										<td>'.$myFileName.'</td>
										<td>'.readableFileSize($myFileSize).'</td>
										<td>
											<!--<a href="/aodp/Processed/'.$myPathToFile.'">
												<button type="submit">View</button>
											</a>-->
											<button onclick="viewConfirmation(\'/aodp/Processed/'.$myPathToFile.'\')"'.$disableView.'>View</button>
										</td>
										<td>
											<a href="/aodp/Processed/'.$myPathToFile.'" download="'.$myFileName.'">
												<button type="submit"'.$disableDownload.'>Download</button>
											</a>
										</td>
									</tr>');
			}
		}
		
		fwrite($myTemplate, '
								</tbody>
							</table>
						</td>
					</tr>
				</table>
				
				<h1>Regulated Species</h1>
				<a href="#" id="exportButton" onClick="javascript:fnExcelReport();">
					<button>Export Current Table (Excel)</button>
				</a>
		');
	
		if ($genus->hasChildNodes()){
			
			fwrite($myTemplate, '
				<table id="regulatedSpeciesTable" cellspacing="1" class="tablesorter">
					<thead>
						<tr>
							<th>Original Name</th>
							<th>Scientific Name</th>
							<th>Disease</th>
							<th>Country/Organization</th>
						</tr>
					</thead>
					<!--
					<tfoot>
						<tr>
							<th>Original Name</th>
							<th>Scientific Name</th>
							<th>Disease</th>
							<th>Country/Organization</th>
						</tr>
					</tfoot>
					
					<tbody>
					-->
			');
			
			#Regulated species
			$i = 0;
			foreach ($genus->getElementsByTagName('regulatedSpecie') AS $regulatedSpecie) {
				$i++;
				fwrite($myTemplate, '
							<tr>
								<td><i>'.preg_replace('/f.sp./', 'f. sp.', $regulatedSpecie->nodeValue).'</i></td>
								<td><i>'.preg_replace('/f.sp./', 'f. sp.', $regulatedSpecie->getAttribute('scientificName')).'</i></td>
								<td>'.$regulatedSpecie->getAttribute('disease').'</td>
								<td>'.$regulatedSpecie->getAttribute('organizations').'</td>
							</tr>
				');
			}
			
			fwrite($myTemplate, '
					</tbody>
				</table>
				
				<div id="pager" class="pager">
					<!--<form>
						<img src="../images/first.png" class="first"/>
						<img src="../images/prev.png" class="prev"/>
						<input type="text" class="pagedisplay"/>
						<img src="../images/next.png" class="next"/>
						<img src="../images/last.png" class="last"/>
						<select class="pagesize">
							<option selected="selected"  value="10">10</option>
							<option value="20">20</option>
							<option value="30">40</option>
							<option value="'.$i.'">All</option>
						</select>
					</form>-->
					
					<a href="#menu-bar">
						<button>Back to top</button>
					</a>
				</div>
			</div>
			');
		}
		
		fwrite($myTemplate, '
					<footer><p>Copyright &copy;2016 AAFC All Rights Reserved.</p></footer>
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
		function viewConfirmation(path) {
				var r = confirm("This file may be too large to view.\nDo you still want to view it?");
				if (r == true) {
					window.open(
						path,
						"_blank"
					);
				}
		}
		</script>
		
	</body>
</html>
		');
		
		fclose($myTemplate);
	}
}

function readableFileSize($bytes, $decimals = 2) {
    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}
?>