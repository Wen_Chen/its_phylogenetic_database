#!/usr/bin/perl
use strict;
use warnings;
use Cwd;

use IO::File;
use FindBin;
use lib "$FindBin::Bin/../modules";
use Writer;

my @ignoreDirs = ("download_seq", "raw");
my @ignoreFolders = ("Summary", "Preperations");
my @ignoreFiles;
my @summaryIgnoredFiles;

my $homedir = getcwd();

opendir (my $dh, $homedir) || die "Can't open $homedir: $!";

my @dirs = grep { -d && !(/^\./) && !(/download_seq/) && !(/raw/) } readdir($dh);

my $filename = "aodpFilePaths.xml";

my $xml = new IO::File(">$filename");

my $writer = Writer->new(OUTPUT => $xml, DATA_MODE => 1, DATA_INDENT => 2);

$writer->xmlDecl('UTF-8');

$writer->startTag("aodp",
									"xmlns:xsi" => "http://www.w3.org/2001/XMLSchema-instance");
									#"xsi:noNamespaceSchemaLocation" => "aodp.xsd"
$writer->startTag("path");
$writer->characters("$homedir");
$writer->endTag("path");

$writer->startTag("host");
$writer->characters("\@biocluster.agr.gc.ca");
$writer->endTag("host");

my $tableMetaData = shift; #input file
my %regulatedSpeciesTable = regulatedSpeciesHash($tableMetaData);

#for each genus directory
foreach my $genus (@dirs) {
	if (!($genus ~~ @ignoreDirs)) {
		$writer->startTag("genusFolder", "name" => "$genus");
		
		opendir (my $genusDir, $genus) || die "can't open $genus: $!";
		chdir $genus;
		
		my @headers = ("Number of Species with Sequences", "Number of Regulated Species in Given List", "Number of Regulated Species Including Synonyms", "Number of Regulated Species with Sequences", "Number of Regulated Species with Oligos", "Number of Regulated Species without Oligos", "Number of Non-Regulated Species with Oligos", "Number of Non-Regulated Species without Oligos", "Total Number of Oligos", "Species Level Oligos", "Regulated Species Oligos", "Total Number of Sequences", "Regulated Species Sequences");
		open (INPUT, "$homedir/$genus/Summary/Overall_summary"); #|| die "Can't open $homedir/$genus/Summary/Overall_summary: $!";
		
		my $line = <INPUT>; #remove headers
		$line = <INPUT>;
		chomp $line;
		my @stats = split(/\t/, $line);
		
		$writer->startTag("stats");
		
		my $i = 0;
		foreach my $stat (@stats) {
			$writer->startTag("stat");
			$writer->characters($headers[$i]."\t".$stat);
			$writer->endTag("stat");
			$i++;
		}
		$writer->endTag("stats");
		
		close INPUT;
		
		$writer->startTag("regulated");
		
		#print Dumper(@{$regulatedSpeciesTable{$genus}{Synonyms}});
		
		foreach my $genus (@{$regulatedSpeciesTable{$genus}{Synonyms}}){
			my @species = keys %{$regulatedSpeciesTable{$genus}{Species}};
			foreach my $specie (@species){
				$writer->startTag("regulatedSpecie",
													"disease" => "$regulatedSpeciesTable{$genus}{Species}{$specie}{Disease}",
													"scientificName" => "$regulatedSpeciesTable{$genus}{Species}{$specie}{sName}",
													"organizations" => "$regulatedSpeciesTable{$genus}{Species}{$specie}{Organizations}");
				$writer->characters("$specie");
				$writer->endTag("regulatedSpecie");
			}
		}
		$writer->endTag("regulated");
		
		
		
		my @subdirs = grep { -d && !(/^\./)  } readdir($genusDir);
		
		#for each sub-directory under genus directory
		foreach my $subdir (@subdirs) {
			if (!($subdir ~~ @ignoreFolders)) {
				my $dirType = (split (/_/, $subdir))[1];
				$writer->startTag($dirType."Folder", "name" => "$subdir");
				
				opendir (my $subDir, $subdir) || die "Can't open $subdir: $!";
				chdir $subdir;
				
				my @files = grep { -f } readdir($subDir);
				@files = sort @files;
				
				#for each file in sub-directory
				foreach my $file (@files) {
					my $size = -s $file;
					if (!($file ~~ @ignoreFiles)) {
						
						$writer->startTag("file",
															"fileName" => "$file",
															"fileSize" => "$size");
						$writer->endTag("file");
					}
				}
				closedir $subDir;
				$writer->endTag($dirType."Folder");
				chdir "$homedir/$genus";
			}
		}
		
		closedir $genusDir;
		$writer->endTag("genusFolder");
		chdir $homedir;
	}
}

$writer->endTag("aodp");
$writer->end();
$xml->close();

closedir $dh;

sub regulatedSpeciesHash {
	my %hash;

	open (INPUT, "<$_[0]") || die "Can't open $_[0]: $!";

	while (my $line = <INPUT>) {
		chomp $line;
		
		my @array = split(/,/, $line);
		
		#synonyms for the Genus
		my @synonyms = split(/ /, $array[1]);
		
		#final name
		my $final = join(" ", split(/ /, $array[3]));
		
		#organizations/countries
		my $organizations;
		if ($array[5]) {
			$organizations = join(", ", split(/\t/, $array[5]));
		}
		
		@{$hash{$array[0]}{Synonyms}} = @synonyms;
		$hash{$array[0]}{Species}{$array[2]}{sName} = $final;
		$hash{$array[0]}{Species}{$array[2]}{Disease} = $array[4];
		$hash{$array[0]}{Species}{$array[2]}{Organizations} = $organizations;
		
		#print Dumper($hash{$array[0]});
	}
	
	close INPUT;
	return %hash;
}











