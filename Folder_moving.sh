#!/bin/bash

Path=$1

mkdir AODP_input
mv *fix.fasta AODP_input
mv *MAFFT-direction.FastTree.tre AODP_input

mkdir AODP_output
mv *lineage AODP_output
mv *newick  AODP_output
mv *node-list AODP_output
mv *.oligo.* AODP_output
mv *.eps AODP_output

mkdir Preperations
mv *.NE* Preperations
mv *.fasta Preperations
mv qsub_log Preperations
mv *summary.txt Preperations
mv *.problematic.txt Preperations
mv *no_detections* Preperations
mv *.graph Preperations


mkdir Summary
mv Overall_summary Summary
mkdir Summary/Oligos_results
mv *_Oligos Summary/Oligos_results/
mv *_Summary Summary/Oligos_results/

rm -rf *.e* *.o* *.pe* *.po* *error.txt qsub*

echo -e "done" >> $Path/Genus_progress
