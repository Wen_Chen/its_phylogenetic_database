#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "IndexFungorum"
#$ -S /bin/bash

reg_spe_list=$1;
oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";

d=`date +%Y%m%d`;

if [ -d "$oligo/Index_fungorum_Summary" ]; then
	mkdir $oligo/Index_fungorum_Summary/IF_outcome_$d;
else
	mkdir $oligo/Index_fungorum_Summary;
	mkdir $oligo/Index_fungorum_Summary/IF_outcome_$d;
fi

perl $script/indexfungorum_API_species.pl $oligo/$reg_spe_list $oligo/Index_fungorum_Summary/IF_outcome_$d

