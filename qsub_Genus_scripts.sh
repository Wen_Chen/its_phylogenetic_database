#!/bin/bash

qsub qsub_Genus_ITSx.sh
cat qsub_Genus_ITSx.sh >> qsub_log

qsub -hold_jid Genus_QC qsub_Genus_MAFFT.sh
cat qsub_Genus_MAFFT.sh >> qsub_log

qsub -hold_jid Genus_MAFFT qsub_Genus_check_MAFFT.sh
cat qsub_Genus_check_MAFFT.sh >> qsub_log

qsub -hold_jid Genus_MAFFT_fix qsub_Genus_fastTree.sh
cat qsub_Genus_fastTree.sh >> qsub_log

qsub -hold_jid Genus_fastTree qsub_Genus_AODP2.sh
cat qsub_Genus_AODP2.sh >> qsub_log

qsub -hold_jid Genus_AODP qsub_Genus_oligo_species_coverage.sh
cat qsub_Genus_oligo_species_coverage.sh >> qsub_log

qsub -hold_jid Genus_oligo_species_coverage qsub_Genus_overall_summary.sh
cat qsub_Genus_overall_summary.sh >> qsub_log

qsub -hold_jid Genus_overall_summary qsub_Genus_folder_arrangement.sh
cat qsub_Genus_folder_arrangement.sh >> qsub_log
