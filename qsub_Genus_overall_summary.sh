#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Genus_overall_summary"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

perl $script/Overall_summary.pl Species_Oligos_Summary Genus.oligo.tab Genus_regulated_species.txt $raw/IF_NCBI_Taxonomy_Summary.txt Genus Genus.QC.fasta
