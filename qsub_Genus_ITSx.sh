#!/bin/bash
#$ -q all.q
#$ -pe smp 10
#$ -cwd
#$ -N "Genus_QC"
#$ -S /bin/bash

perl /isilon/biodiversity/users/oligo_database/ITS_database_base_scripts/ITSx.pl Genus.fasta Genus /isilon/biodiversity/users/oligo_database/ITS_database_base_scripts/ITSx_1.0.11/ITSx $NSLOTS

