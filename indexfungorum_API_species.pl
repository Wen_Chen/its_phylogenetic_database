#!/usr/bin/env perl

use strict;
use warnings;

#use module;
use Bio::SeqIO;
use File::Basename;
use File::Copy;
use File::Spec;
use Scalar::Util qw/reftype/;
use Cwd;
use utf8;
use feature 'unicode_strings';
use HTML::Entities;
use open ':std', ':encoding(utf-8)';
use Unicode::Normalize; #decompose the characters
use XML::Simple;
use Data::Dumper;

use FindBin;
use lib "$FindBin::Bin/Modules";
use Text::Unidecode;

binmode STDOUT, ':utf8';

my $species_list=shift or die "taxa list?";
my $outpath=shift;
my $prefix=(split /\//, $species_list)[-1];

################################################################
# check full name in pathogen list, keep only Genus_species only
################################################################

open(F, "<$species_list");
#binmode(F, ":utf8");
open(OUT_sum, ">$outpath/$prefix\_IF_species_summary.txt");
open (OUT, ">$outpath/$prefix\_IF_species_xml_parsed.txt");
open (OUT_cur, ">$outpath/$prefix\_IF_species_current_name_xml_parsed.txt");
binmode(OUT_sum, ":utf8");
binmode(OUT_cur, ":utf8");
binmode(OUT, ":utf8");

print OUT_sum "Species_original\tTotal_species\tAccepted_species_with_same_genus_name\tAccepted_species_with_different_genus_name\n";
print OUT "Species_original\tSpecies_Name\tAccepted_Name\tAccepted_Record_num\tAuthority\tYear\tPublication\tTypification\n";
print OUT_cur "Species_original\tSpecies_Name\tAccepted_Name\tAuthority\tYear\tPublication\tTypification\tAccepted_Record_num\n";

my $count_s;
my @spec;
my @gen;
my %genera=();
my @genera_syn;
while (<F>) {
  chomp $_;
  my @n = split /[ ]+/, $_;
  unless ( $_ eq '') {
    push (@gen, $n[0]);
    push (@spec, $_);
    $count_s++;
  }
}

my @gen_uniq = do { my %seen; grep { !$seen{$_}++ } @gen };
foreach my $g (@gen_uniq) {
  $genera{$g}=$g
}


#foreach my $t (sort keys %genera) {
#  print $t, "\t", $genera{$t}, "\n";
#}
#exit;

#print join(", ", @spec), "\n\n";

my @norecord; 
my @genus; 
my $count_es;
# generic_names hash: keys: original, synonymous
my %generic_names=();
foreach my $species (sort @spec) {

  chomp $species;
  print $species, "\n";
  my $spe=$species;
  my @name_orig=split /[ ]+/, $species;
  if ( scalar @name_orig == 1 ) {
    $species = $name_orig[0];
  } elsif ( (scalar @name_orig > 1) && ( $name_orig[1] eq "spp." || $name_orig[1] eq "spp" || $name_orig[1] eq "sp")) {
    $species = $name_orig[0]; # use generic name for search
  } else {
    $species = $species;
  }

  my $gen_orig=$name_orig[0];
  # create array for generic synonymous names;
  my @gen_syn;



  $spe=~ s/[ ]+/_/g;
  my $count_n;
  my $count_cn;
  my @ansp_ss; # Accepted_species_with_same_speces_name
  my @ansp_ds; # Accepted_species_with_different_species_name
  my @nsp;
  $count_es++;
  #print $species, "\n"; 
  # logic in shell script which will retry it to run again after 15 sec upto 10 times based on "status code=FAIL" if it fails due to some issue
  system(`for i in {1..10}; do curl --data 'SearchText=$species&AnywhereInText=FALSE&MaxNumber=5000' http://www.indexfungorum.org/ixfwebservice/fungus.asmx/NameSearch -H "Content-Type: application/x-www-form-urlencoded" --use-ascii > "$outpath/$spe.IF.xml"  && break ||sleep 15; done; `);

  print "\n";

  # create object
  my $xml = new XML::Simple (KeyAttr=>[]);

  # read XML file
  my $data = $xml->XMLin("$outpath/$spe.IF.xml");

  # dereference hash ref
  # access <employee> array
  my @out= @{if_array($data->{IndexFungorum})};
  if ( @out ) {
    print scalar @out, " record(s) for $species! \n\n";
    my $s_count; # number of species names matched
    my $ss_count;# number of species with same generic name
    my $sd_count; # number of species with different generic name

    foreach my $s (@{if_array($data->{IndexFungorum})}){
      my $n;
      my $cn;
      my $cn_rn; # current name record number;
      my $au;
      my $y;
      my $pu;
      my $ty;
      
      if (defined $s->{"NAME_x0020_OF_x0020_FUNGUS"} ) {
        $n = $s->{"NAME_x0020_OF_x0020_FUNGUS"};
        $n=~ s/[ ]+/ /g;
        print $n, "\n";

        unless ( $n eq '' ) {
          my @name=split /[ ]+/, $n;
          my $fulln;
          if ( scalar @name ==1 ) {
            $fulln = $name[0];
          } else {
            $fulln=join(" ", $name[0], $name[1]);
            $fulln=~ s/\s+$//g;
          }
          #print $n, "\t", $fulln, "\n\n";binmode STDOUT, ':utf8';
          if (scalar @name == 1 ) { 
            next; 
          } elsif (defined $name[0] ) {
            #print unidecode($species), "\t", unidecode($fulln), "\t", unidecode($name[0]), "\n\n";
            if ( (unidecode($species) eq unidecode($fulln)) || (unidecode($species) =~ m/unidecode($fulln)\s/) || (unidecode($name[0]) eq unidecode($species)) ) {
            #if ( ($species eq $fulln)  ) {
              print unidecode($species), "\t", unidecode($fulln), "\t", unidecode($name[0]), "\n\n";
              print $n, "\t", $fulln, "\n\n";
              $s_count++;
              $count_es++;
              push (@nsp, join(" ", $name[0], $name[1]));
              $count_n++;
              push (@gen_syn, $name[0]);
              #print join(", ", @gen_syn), "\n";

              
              if (defined ($s->{"CURRENT_x0020_NAME"}) || defined ($s->{"ORTHOGRAPHY_x0020_COMMENT"}) ) {

                if (defined ($s->{"CURRENT_x0020_NAME"})) {
                  $cn=$s->{"CURRENT_x0020_NAME"}; 
                } elsif ( !defined ($s->{"CURRENT_x0020_NAME"}) && defined ($s->{"ORTHOGRAPHY_x0020_COMMENT"})) {
                  my $on = $s->{"ORTHOGRAPHY_x0020_COMMENT"};
                  $on=~ s/[ ]+/ /g;
                  my @field = split / /, $on;
                  if ( scalar @field > 1) {
                    $cn=$s->{"ORTHOGRAPHY_x0020_COMMENT"}; 
                  } else {
                    $cn="";
                  }
                } else {
                  $cn=""
                }
     
                print $cn, "\n";
                $cn=~ s/[ ]+/ /g;
                $cn_rn=$s->{"CURRENT_x0020_NAME_x0020_RECORD_x0020_NUMBER"};

                if (defined ($cn_rn) ) {
                  syn_of_current($cn_rn);
                  my @records=parse_xml("$cn_rn.IF.xml");
                  foreach my $re (@records) {
                    $re =~ s/([^[:ascii:]]+)/unidecode($1)/ge;
                    print OUT_cur $species, "\t", $re, "\t", $cn_rn,"\n";
                  }               
                  print scalar @records, " synonyms for $cn! \n\n";
                }
                
                my @f=split /[ ]+/, $cn;
                push (@gen_syn, $f[0]);
                if ( ( defined $f[0]) && ( uc(unidecode($f[0])) eq uc(unidecode($gen_orig)) ) ) {
                  $ss_count++;
                  
                  #push (@ansp_ss, join(" ", $f[0], $f[1]));
                  print "Generic name of $species is same as that of the current name \n";
                } elsif ( (defined $f[0]) && ( uc(unidecode($f[0])) ne uc(unidecode($gen_orig)) ) ) {
                  $sd_count++;
                  push (@ansp_ds, join(" ", $f[0], $f[1]));
                  print "Generic name of $species is different from that of the current name \n";
                } else {
                  $cn="";
                  $cn_rn="";
                }
              } else {
                $cn="";  
                $cn_rn="";
                $ss_count++;
                push (@gen_syn, "");             
              } 
              #print join(", ", @gen_syn), "\n";

              if (defined $s->{"AUTHORS"} ) {
                $au=$s->{"AUTHORS"}; 
                $au=~ s/[_]+/ /g;
              } else {
                $au=""; 
              }

              if (defined $s->{"YEAR_x0020_OF_x0020_PUBLICATION"} ) {
                $y=$s->{"YEAR_x0020_OF_x0020_PUBLICATION"}; 
                $y=~ s/[_]+/ /g;
              } else {
                $y=""; 
              }

              if (defined $s->{"PUBLISHED_x0020_LIST_x0020_REFERENCE"} ) {
                $pu=$s->{"PUBLISHED_x0020_LIST_x0020_REFERENCE"}; 
                $pu=~ s/[_]+/ /g;
              } else {
                $pu=""; 
              }

              if (defined $s->{"TYPIFICATION_x0020_DETAILS"} ) {
                $ty=$s->{"TYPIFICATION_x0020_DETAILS"}; 
                $ty=~ s/[_]+/ /g;
              } else {
                $ty=""; 
              }

              print "Species: ", $species, "\n";
              print "Species NAME: ", $n, "\n";
              print "CURRENT Species NAME: ", $cn, "\n"; 
              print "AUTHORITY: ", $au, ", ",  $y, ", ", $pu, "\n";
              print "TYPE: ", $ty, "\n";
              print "\n";
      
              print OUT $species, "\t", $n, "\t", $cn, "\t", $cn_rn, "\t", $au, "\t", $y, "\t", $pu, "\t", $ty, "\n"; 

            } else {
              next;
            }
          } else {
            next;
          }
        
        } else {
          next;
        }
        
      }
 
    #my @ansp_ss_uniq = do { my %seen; grep { !$seen{$_}++ } @ansp_ss };
    #my @ansp_ds_uniq = do { my %seen; grep { !$seen{$_}++ } @ansp_ds };
    #my @nsp_uniq = do { my %seen; grep { !$seen{$_}++ } @nsp };
    }
    if (defined $s_count) {
      $s_count= $s_count
    } else {
      $s_count= 0;
    }

    if (defined $ss_count) {
      $ss_count= $ss_count
    } else {
      $ss_count= 0;
    }
    
    if (defined $sd_count) {
      $sd_count= $sd_count
    } else {
      $sd_count= 0;
    }


   print OUT_sum $species, "\t", $s_count, "\t", $ss_count, "\t", $sd_count, "\n";  
  

  } else {
    print "NO record for $species! \n\n";
    push (@norecord, $species);
    next;
  }

  
  my @gen_syn_uniq;
  @gen_syn = grep { defined $_ } @gen_syn;
  if (@gen_syn) {
    @gen_syn_uniq = do { my %seen; grep { !$seen{$_}++ } @gen_syn };
  } else {
    @gen_syn_uniq=();
  }   
  print join(", ", @gen_syn_uniq), "\n";
  @{$generic_names{$spe}} = @gen_syn_uniq;


  #print join(", ", @gen_syn1), "\n"; 
  #@{$generic_names{"synonymous"}} = @gen_syn_uniq;
  #print join(", ", @gen_syn_uniq), "\n";

  #for my $k (keys %generic_names) {
  #  print $k, ": ", join(", ", @{$generic_names{$k}}), "\n";
  #}

  
}

close F;

#print "\nSearched $count_s taxa in Index Fungorum\nParsed xml file is saved to: $species_list\_IF_species_xml_parsed.txt\n";
#print "Summary of species count is saved to: $species_list\_IF_species_summary.txt\n\n";
#system(`rm *.xml`);

#open (OUT, ">$outpath/$prefix\_IF_generic_synonyms_of_species.txt");
#binmode(OUT, ":utf8");
#foreach my $k (keys %generic_names) {
#  print OUT $k, ": ", join(", ", @{$generic_names{$k}}), "\n";
#}
#close OUT;

#open (OUT, ">$outpath/$prefix\_IF_norecord.txt");
#binmode(OUT, ":utf8");
#foreach my $ne (@norecord) {
#  print OUT $ne, "\n";
#}
#close OUT;

#exit;
##############################################
# check norecord, keep only Genus_species only
###############################################

my @norecord1; 
my @genus1; 
my $count_es1;
# generic_names hash: keys: original, synonymous
my %generic_names1=();
foreach my $species1 (sort @norecord) {

  chomp $species1;
  my $spe=$species1;
  my @name_orig=split /[ ]+/, $species1;
  my $species = join(" ", $name_orig[0], $name_orig[1]);
  
  my $gen_orig=$name_orig[0];
  # create array for generic synonymous names;
  my @gen_syn;

  $spe=~ s/[ ]+/_/g;
  my $count_n;
  my $count_cn;
  my @ansp_ss; # Accepted_species_with_same_speces_name
  my @ansp_ds; # Accepted_species_with_different_species_name
  my @nsp;
  $count_es1++;

  print $species, "\n"; 
  system(`for i in {1..10}; do curl --data 'SearchText=$species&AnywhereInText=FALSE&MaxNumber=5000' http://www.indexfungorum.org/ixfwebservice/fungus.asmx/NameSearch -H "Content-Type: application/x-www-form-urlencoded" --use-ascii > "$outpath/$spe.IF.xml"  && break ||sleep 15; done; `);

  print "\n";

  # create object
  my $xml = new XML::Simple (KeyAttr=>[]);

  # read XML file
  my $data = $xml->XMLin("$outpath/$spe.IF.xml");

  # dereference hash ref
  # access <employee> array
  my @out= @{if_array($data->{IndexFungorum})};
  if ( @out ) {
    print scalar @out, " record(s) for $species! \n\n";
    my $s_count; # number of species names matched
    my $ss_count;# number of species with same generic name
    my $sd_count; # number of species with different generic name

    foreach my $s (@{if_array($data->{IndexFungorum})}){
      my $n;
      my $cn;
      my $cn_rn; # current name record number
      my $au;
      my $y;
      my $pu;
      my $ty;
      
      if (defined $s->{"NAME_x0020_OF_x0020_FUNGUS"} ) {
        $n = $s->{"NAME_x0020_OF_x0020_FUNGUS"};
        $n=~ s/[ ]+/ /g;

        unless ( $n eq '' ) {
          my @name=split /[ ]+/, $n;
          my $fulln;
          if ( scalar @name ==1 ) {
            $fulln = $name[0];
          } else {
            $fulln=join(" ", $name[0], $name[1]);
            $fulln=~ s/\s+$//g;
          }
          #print $n, "\t", $fulln, "\n\n";
          if (scalar @name == 1 ) { 
            next; 
          } elsif (defined $name[0] ) {
            if ( ($species eq $fulln) || ($species =~ m/$fulln\s/) || ($name[0] eq $species) ) {
            #if ( ($species eq $fulln)  ) {
              print $n, "\t", $fulln, "\n\n";
              $s_count++;
              $count_es++;
              push (@nsp, join(" ", $name[0], $name[1]));
              $count_n++;
              push (@gen_syn, $name[0]);
              #print join(", ", @gen_syn), "\n";

              if (defined ($s->{"CURRENT_x0020_NAME"}) || defined ($s->{"ORTHOGRAPHY_x0020_COMMENT"}) ) {

                if (defined ($s->{"CURRENT_x0020_NAME"})) {
                  $cn=$s->{"CURRENT_x0020_NAME"}; 
                } elsif ( !defined ($s->{"CURRENT_x0020_NAME"}) && defined ($s->{"ORTHOGRAPHY_x0020_COMMENT"})) {
                  my $on = $s->{"ORTHOGRAPHY_x0020_COMMENT"};
                  $on=~ s/[ ]+/ /g;
                  my @field = split / /, $on;
                  if ( scalar @field > 1) {
                    $cn=$s->{"ORTHOGRAPHY_x0020_COMMENT"}; 
                  } else {
                    $cn="";
                  }
                } else {
                  $cn=""
                }
                                
                print $cn, "\n";
                $cn=~ s/[ ]+/ /g;
                $cn_rn=$s->{"CURRENT_x0020_NAME_x0020_RECORD_x0020_NUMBER"};
                if (defined ($cn_rn) ) {
                  syn_of_current($cn_rn);
                  my @records=parse_xml("$cn_rn.IF.xml");
                  foreach my $re (@records) {
                    $re =~ s/([^[:ascii:]]+)/unidecode($1)/ge;
                    print OUT_cur $species, "\t", $re, "\t", $cn_rn,"\n";
                  }               
                  print scalar @records, " synonyms for $cn! \n\n";
                }

                my @f=split /[ ]+/, $cn;
                push (@gen_syn, $f[0]);
                if ( ( defined $f[0]) && ( uc($f[0]) eq uc($gen_orig) ) ) {
                  $ss_count++;
                  #push (@ansp_ss, join(" ", $f[0], $f[1]));
                  print "Generic name of $species is same as that of the current name \n";
                } elsif ( (defined $f[0]) && ( uc($f[0]) ne uc($gen_orig) ) ) {
                  $sd_count++;
                  push (@ansp_ds, join(" ", $f[0], $f[1]));
                  print "Generic name of $species is different from that of the current name \n";
                } else {
                  $cn="";
                  $cn_rn="";
                }
              } else {
                $cn="";  
                $cn_rn="";
                $ss_count++;
                push (@gen_syn, "");             
              } 
              #print join(", ", @gen_syn), "\n";

              if (defined $s->{"AUTHORS"} ) {
                $au=$s->{"AUTHORS"}; 
                $au=~ s/[_]+/ /g;
              } else {
                $au=""; 
              }

              if (defined $s->{"YEAR_x0020_OF_x0020_PUBLICATION"} ) {
                $y=$s->{"YEAR_x0020_OF_x0020_PUBLICATION"}; 
                $y=~ s/[_]+/ /g;
              } else {
                $y=""; 
              }

              if (defined $s->{"PUBLISHED_x0020_LIST_x0020_REFERENCE"} ) {
                $pu=$s->{"PUBLISHED_x0020_LIST_x0020_REFERENCE"}; 
                $pu=~ s/[_]+/ /g;
              } else {
                $pu=""; 
              }

              if (defined $s->{"TYPIFICATION_x0020_DETAILS"} ) {
                $ty=$s->{"TYPIFICATION_x0020_DETAILS"}; 
                $ty=~ s/[_]+/ /g;
              } else {
                $ty=""; 
              }

              print "Species: ", $species, "\n";
              print "Species NAME: ", $n, "\n";
              print "CURRENT Species NAME: ", $cn, "\n"; 
              print "AUTHORITY: ", $au, ", ",  $y, ", ", $pu, "\n";
              print "TYPE: ", $ty, "\n";
              print "\n";
      
              print OUT $species, "\t", $n, "\t", $cn, "\t", $cn_rn, "\t", $au, "\t", $y, "\t", $pu, "\t", $ty, "\n"; 

            } else {
              next;
            }
          } else {
            next;
          }
        
        } else {
          next;
        }
        
      }
 
    #my @ansp_ss_uniq = do { my %seen; grep { !$seen{$_}++ } @ansp_ss };
    #my @ansp_ds_uniq = do { my %seen; grep { !$seen{$_}++ } @ansp_ds };
    #my @nsp_uniq = do { my %seen; grep { !$seen{$_}++ } @nsp };
    }
    if (defined $s_count) {
      $s_count= $s_count
    } else {
      $s_count= 0;
    }

    if (defined $ss_count) {
      $ss_count= $ss_count
    } else {
      $ss_count= 0;
    }
    
    if (defined $sd_count) {
      $sd_count= $sd_count
    } else {
      $sd_count= 0;
    }


    print OUT_sum $species, "\t", $s_count, "\t", $ss_count, "\t", $sd_count, "\n";  
  
    
    
   my @gen_syn_uniq;
   @gen_syn = grep { defined $_ } @gen_syn;
   if ( @gen_syn) {
     my @gen_syn_uniq = do { my %seen; grep { !$seen{$_}++ } @gen_syn };
     print join(", ", @gen_syn_uniq), "\n";
   } else {
      @gen_syn_uniq=();
   }
   @{$generic_names{$spe}} = @gen_syn_uniq;


   #print join(", ", @gen_syn1), "\n"; 
   #@{$generic_names{"synonymous"}} = @gen_syn_uniq;
    #print join(", ", @gen_syn_uniq), "\n";

    #for my $k (keys %generic_names) {
    #  print $k, ": ", join(", ", @{$generic_names{$k}}), "\n";
    #}


  } else {
    print "NO record for $species! \n\n";
    push (@norecord1, $species);
    next;
  }
  
}

close OUT_sum;
close OUT;
#print "\nSearched $count_s taxa in Index Fungorum\nParsed xml file is saved to: $species_list\_IF_norecord_xml_parsed.txt\n";
#print "Summary of species count is saved to: $species_list\_IF_norecord_summary.txt\n\n";
#system(`rm *.xml`);

open (OUT, ">$outpath/$prefix\_IF_norecord_final.txt");
binmode(OUT, ":utf8");
foreach my $ne1 (@norecord1) {
  print OUT $ne1, "\n";
}
close OUT;


####################################################
# combine generic synonymous names
####################################################
# merge hashes

#my %newHash = (%generic_names, %generic_names1);

my %newHash;
foreach my $gns ( keys %generic_names ){
    if (exists $generic_names1{$gns}) {
        my @all = [@{$generic_names{$gns}}, @{$generic_names1{$gns}}];
        my @uniq = do { my %seen; grep { !$seen{$_}++ } @all };
        $newHash{$gns} = @uniq;
    } else {
        @{$newHash{$gns}} = @{$generic_names{$gns}}
    }
}

foreach my $gns1 ( keys %generic_names1 ){
    if (exists $newHash{$gns1}) {
      next;
    } else {
      @{$newHash{$gns1}} = @{$generic_names1{$gns1}}
    }
}

# print all generic synonyms of species
my @exitg;
open (OUT, ">$outpath/$species_list\_IF_generic_synonyms_of_species_final.txt");
binmode(OUT, ":utf8");

foreach my $t (sort keys %newHash) {
  my @kgs= split /[_]+/, $t;
  push @exitg, $kgs[0];
  print OUT $t, "\t", join(", ", @{$newHash{$t}}), "\n";
}
close OUT;

my @uniq_exitg = do { my %seen; grep { !$seen{$_}++ } @exitg };

print "Total ", scalar @uniq_exitg, " genera found in IndexFungorum records\n";


# total generic synonyms of each genus
my %genera_final;
foreach my $gn (@uniq_exitg) {
  my @all;
  foreach my $ke (sort keys %newHash) {
    my @kgs= split /[_]+/, $ke;
    if ($kgs[0] eq $gn) {
      #print $kgs[0], "\n";
      #print $kgs[0], "\t", join(",",@{$newHash{$ke}}), "\t", $kgs[0], "\n";
      push (@all, @{$newHash{$ke}});      
    } else {
      next;
    }
  }
  my @uniq = do { my %seen; grep { !$seen{$_}++ } @all };
  if (@uniq) {
    @{$genera_final{$gn}} = @uniq;
  } else {
    next;
  }
}

open (OUT, ">$outpath/$species_list\_IF_genera_synonyms_final.txt");
binmode(OUT, ":utf8");
print OUT "Generic_name_in_pathogen_list", "\t", "Synonymous_Generics_names_in_IF", "\n";
foreach my $t (sort keys %genera_final) {
  print OUT $t, "\t", join(", ", @{$genera_final{$t}}), "\n";
}

## remove xml files
foreach ( glob("./*.xml") ) {
  unlink $_ if (-f $_ && /\.xml/);
}



###########################################
### subroutine to keep Array type variable
###########################################
sub if_array {
    my $returnValue = shift;

    if (defined($returnValue) && ref($returnValue) eq "ARRAY"){
      return($returnValue);      
    } elsif(defined($returnValue) && ref($returnValue) ne "ARRAY") {
       return( [$returnValue] );
    } else {
       return ([]);
    } 
}

sub syn_of_current {
  my $cn_rn=shift;
  if (defined $cn_rn ) {
    print $cn_rn, "\n"; 
    system(`for i in {1..5}; do curl http://www.indexfungorum.org/ixfwebservice/fungus.asmx/NamesByCurrentKey?CurrentKey=$cn_rn -H "Content-Type: application/x-www-form-urlencoded" --use-ascii > "$cn_rn.IF.xml" && break ||sleep 15; done `);
  } else {
    print $cn_rn " doesn't exist in Index Fungorum!\n"
  }
}
  

sub parse_xml {
  my $xml_file = shift;
  # create object
  my $xml = new XML::Simple (KeyAttr=>[]);

  # read XML file
  my $data = $xml->XMLin("$xml_file");

  my @records;
  # dereference hash ref
  # access <employee> array
  my @out= @{if_array($data->{IndexFungorum})};
  if ( @out ) {
    
    foreach my $s (@{if_array($data->{IndexFungorum})}){
      my $n;
      my $cn;
      my $au;
      my $y;
      my $pu;
      my $ty;
      
      if (defined $s->{"NAME_x0020_OF_x0020_FUNGUS"} ) {
        $n = $s->{"NAME_x0020_OF_x0020_FUNGUS"};
        $n=~ s/[ ]+/ /g;
      } else {
        $n="";
      }

      if (defined $s->{"CURRENT_x0020_NAME"} ) {
        $cn=$s->{"CURRENT_x0020_NAME"}; 
        $cn=~ s/[_]+/ /g;           
      } else {
        $cn="";
      }

      if (defined $s->{"AUTHORS"} ) {
         $au=$s->{"AUTHORS"}; 
         $au=~ s/[_]+/ /g;
      } else {
         $au=""; 
      }

      if (defined $s->{"YEAR_x0020_OF_x0020_PUBLICATION"} ) {
         $y=$s->{"YEAR_x0020_OF_x0020_PUBLICATION"}; 
         $y=~ s/[_]+/ /g;
      } else {
         $y=""; 
      }

      if (defined $s->{"PUBLISHED_x0020_LIST_x0020_REFERENCE"} ) {
          $pu=$s->{"PUBLISHED_x0020_LIST_x0020_REFERENCE"}; 
          $pu=~ s/[_]+/ /g;
      } else {
          $pu=""; 
      }

      if (defined $s->{"TYPIFICATION_x0020_DETAILS"} ) {
          $ty=$s->{"TYPIFICATION_x0020_DETAILS"}; 
          $ty=~ s/[_]+/ /g;
      } else {
          $ty=""; 
      }
      
      my $info = join("\t", $n, $cn, $au, $y, $pu, $ty);
      $info =~ s/([^[:ascii:]]+)/unidecode($1)/ge;
      chomp $info;
      push @records, $info; 

    }
        
  } else {
    next;
  }
  return @records; 
}


