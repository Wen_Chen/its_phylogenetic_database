#!/usr/bin/env perl
use warnings;
use strict;
use Bio::SeqIO;
use File::Find;
use File::Spec;
use File::Basename;
use List::MoreUtils qw(first_index);
 

#author: Eric Hardy; Wen Chen <wen.chen@agr.gc.ca>
system("rm error.txt");

my $gb_fasta=shift or die "genbank sequences?";
_reformat($gb_fasta);

#changes sequence data into a standard form
sub _reformat {
  
  my $seq_ref  = shift();
  my @suffix = (".fasta", ".fas", ".fna", ".fa");
  my $basename=basename($seq_ref, @suffix);
  my $gen;
  my $spe;
  my $sub_sp;
  

  open (OUT, ">$basename.NE.fasta");
  open (OUT1, ">> error.txt");
  my @errorID;
  my @goodID;
  my $in=Bio::SeqIO->new(-file=>"$seq_ref", -format=>"fasta");
  my $count=0;
  while (my $seq=$in->next_seq()) {
        $count++;
   		my $id = $seq->id();
 		chomp($id);
        #print $id, "\n";
        #my $newid = $id;
 		 my $new_seq  = $seq->seq();
  		   $new_seq  =~ s/-//g;      #remove dashes (sequence alignment gaps)
   		  #$new_seq  =~ tr/[a-z]/[A-Z]/; #capitalize the sequence

		  #shorten and reformat the description to just species name
 		 my $desc     = $seq->desc();
 		 my @words    = split(/[ |:,]+/, $desc); # get individual words of description
                 #figure out how long the name is supposed to be
 		 my $name_length = 0;
 		 my @new_desc;
         my @want1 = ('var.', 'cf.', 'f.', 'sp.', 'aff.', 'spnov');
         my @want = ("strain", "clone", "isolate", "clade", "vochour", "type", "genotype", "pathotype", "race", 'sect', "ATCC", "AS", 'BMP', "BRIP", "CBS",  'CID', "CCFC", 'CCF', 'CPC', 'CMFISB', "CV", "DAOM", 'DGG', "DTO", 'EEB', 'EGS', "FRR", "IBT", 'ICMP', "IMI", "JCM", "KACC", "LD", 'Mos', 'MAFF', 'NBRC', "NRRL", 'PRPI', 'RGR', 'STE', 'URM');
         if ( $words[0] !~ m/^[\w]+/ ) {
               print OUT1 "Error: ", $id, " ", $desc, "\n";
               push (@errorID, $id);
               #exit;
         } elsif ( $words[0] =~ m/^\w\./ ) {
               print OUT1 "Error: ", $id, " ", $desc, "||";
               print OUT1 "genus name is not in full!", "\n";
               push (@errorID, $id);
               $gen = (split /\./, $words[0])[0];
               $spe = (split /\./, $words[0])[1];           
               #exit;
         } elsif (grep ( /^$words[1]$/, @want1)) {
               print OUT1 "Error: ", $id, " ", $desc, "||";
               print OUT1 "species name is not in full!", "\n";
               push (@errorID, $id);
               $gen = $words[0];
               $spe = "sp";           
               #exit;
         } else {
                push (@goodID, $id);
                $gen = $words[0];
                $spe = $words[1];
         }
         push(@new_desc, join("_", $gen, $spe));
         #print join("|", @words[0..1]), "\n";

         my $index;
         my $index_max;
         my @new_desc2;
         foreach my $search (@want1) {
             #print $search, "\n";
             #$index = grep { $words[$_] eq $search } 0..$#words;
             $index= first_index { $_ eq $search } @words;
             #print $index, "\t", scalar @words, "\n";
             #$index= first_index { $_=~ m/$search/ } @words;
             if (($index > 0) && (scalar @words) >= ($index+2) ) {
                 #print $count, "\t";
                 #print $index, "\n";
                 push(@new_desc2, join("|", @words[($index)..($index+2)]));
             } elsif (($index > 0) && (scalar @words) >= ($index+1) ) {
                 #print $count, "\t";
                 #print $index, "\n";
                 push(@new_desc2, join("|", @words[($index)..($index+1)]));
                        
             } else {
                 push(@new_desc2, "");
             }
          } 

          foreach my $search (@want) {
             #print $search, "\n";
             #$index = grep { $words[$_] eq $search } 0..$#words;
             $index= first_index { $_ eq $search } @words;
             #print $index+2, "\t", scalar @words, "\n";
             #$index= first_index { $_=~ m/$search/ } @words;
             #if (!defined($words[$index+2])) {
             #   print $id, "\n";
             #}
             if (($index > 0) && defined($words[$index+2]) ) {
                 #print $count, "\t";
                 #print $index, "\n";
                 #print $words[$index], "\t", $words[$index+2], "\n";
                 push(@new_desc2, join("|", @words[($index)..($index+2)]));
             } elsif (($index > 0) && defined($words[$index+1]) ) {
                 #print $count, "\t";
                 #print $index, "\n";
                 push(@new_desc2, join("|", @words[($index)..($index+1)]));
                        
             } else {
                 push(@new_desc2, "");
             }
          } 

		  my $new_desc_2 = join("|", @new_desc2); # join by "_"
          push(@new_desc, $new_desc_2);
		
          #create the new name

          my $new_desc_1 = join("|", @new_desc); #new_desc = "<genus>_<species>";
          
          for ($new_desc_1) {
              s/[:]+/:/g;
		      #s/[_+()\-;\.\|\*\/ ]+/_/g;
              s/[_+()\-;\.\*\/ ]+/_/g;
              #s/_18S_ribosomal_RNA_gene_partial_sequence//g;
              #s/_5\.8S_rRNA_gene_and_internal_transcribed_spacers_1_and_2_ITS1//g;
		      s/complete//g;
		      s/genes//g;
		      s/gene//g;
		      s/for//g;
		      s/18S//ig;
		      s/rRNA//g;
		      s/internal//g;
		      s/transcribed//g;
              s/isolate\|isolate/isolate/g;
		      s/spacer//g;
		      s/partial//g;
		      s/RNA//g;
		      s/sequence//g;
		      s/ribosomal//g;
		      s/small//g;
		      s/subunit//g;
		      s/ribosomal//g;
		      s/and//g;
		      s/_+/_/g;
		      s/_+$//g;
              s/_isolate$//g;
              s/:_/:/g;
              s/\|_/\|/g;
              s/[\W]+$//g;
	      }	
          $new_desc_1=~ s/[\|]+/|/g;

          my @nd = split /\|/, $new_desc_1;
          my %seen=();
          my @unique = grep { ! $seen{$_} ++ } @nd;
 		  $new_desc_1 = join("|", @unique); #new_desc = "<genus>_<species>";

          my $new_id   =  join("_", $id, $new_desc_1);
	      $new_id  =~ s/[_]+/_/g;
 	      $new_id   =~ s/_+$//g;                    #remove any trailing "_";
            
           # check how many fields by split "_"
           #my @rec = split/\_/, $new_id;
           #if ( scalar (@rec) < 2) {
           #    print $new_id, " incomplete taxonomic name?", "\n";
           #    exit;
           #} elsif ( scalar (@rec) == 2 ) {
           #    $new_id = join("_", $new_id, "sp");
           #    $new_id = join("_", $new_id, "no_strain_info");
           #    print $new_id, " incomplete taxonomic name?", "\n";
           #} elsif ( scalar (@rec) == 3 ) {
           #    $new_id = join("_", $new_id, "no_strain_info");
           #} elsif (scalar (@rec) >4 ) {
           #    print $new_id, "more than 4 fields, check seqID format", "\n";
           #    exit;
           #} else {
           #    $new_id = $new_id;
           #}
           
           #my @newid = split /\|/, $new_id;
           #my $new_id1 = join("|", $newid[1], $newid[2], $newid[3], $newid[0]);
           #print $new_id1, "\n";
# 		my $in_new = Bio::Seq->new(-id       => $new_id,
#		                            -seq      => $new_seq,
 #                           -alphabet => "dna");
#			my $out=write_seq($in_new);
            	
		print OUT ">",$new_id, "\n", $seq->seq, "\n";
	}
}

