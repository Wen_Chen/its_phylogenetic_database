#!/bin/bash

oligo=${1%/}
script="$oligo/its_phylogenetic_database"
raw="$oligo/raw"

temp=${2%/}
user=$3

scp -r $user@biocluster.agr.gc.ca:$oligo/raw/download_seq ~/$temp
scp $user@biocluster.agr.gc.ca:$script/downloadSeqsFromGenBank.pl ~/$temp
scp $user@biocluster.agr.gc.ca:$script/rename_genbank_sequences.pl ~/$temp
scp $user@biocluster.agr.gc.ca:$script/local_rename_genbank_sequences.sh ~/$temp

sleep 60

perl ~/$temp/downloadSeqsFromGenBank.pl -query ~/$temp/download_seq/genus.query -out ~/$temp/download_seq/
~/$temp/local_rename_genbank_sequences.sh ~/$temp

scp -r ~/$temp/download_seq $user@biocluster.agr.gc.ca:$raw

ssh $user@biocluster.agr.gc.ca "qsub $script/mix_2_Oligo_database_update.sh"

#rm -r ~/$temp
