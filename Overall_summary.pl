#!/usr/bin/perl
use warnings;
use strict;
use List::MoreUtils qw(uniq);
use List::MoreUtils qw(indexes);
use Data::Dumper qw(Dumper);

my $oligosummary = shift();
my $oligotab = shift();
my $species = shift();
my $taxon_mapping = shift();
my $Genus = shift();
my $QC = shift();

open (my $osf, "<", $oligosummary) or die "Could not open Species_Oligos_Summary.";
open (my $otf, "<", $oligotab) or die "Could not open Oligo.tab.";
open (my $sf, "<", $species) or die "Could not open regulated_species.txt.";
open (my $tmf, "<", $taxon_mapping) or die "Could not open taxonomy_mapping.txt.";
open (my $qcf, "<", $QC) or die "Could not open QC.fasta";

my @sp;

while (my $line = <$tmf>) {
	next if $. == 1;

	chomp $line;
	my ($sp_name, $genus) = (split /\t/, $line)[0,1];
	if ($genus eq $Genus) {
		push (@sp, $sp_name);
	}
}			

my $sp_count = 0;
my $reg_list_count = scalar (uniq @sp);
my @reg_list_syn_count = <$sf>;
my $reg_with_seq_count = 0;
my $reg_with_oligo_count = 0;
my $reg_no_oligo_count = 0;
my $not_reg_with_oligo_count = 0;
my $not_reg_no_oligo_count = 0;
my @oligos = <$otf>;
my $sp_oligos = 0;
my $reg_oligos = 0;
my $total_seq = 0;
my $reg_seq = 0;

while (my $line = <$osf>) {
	next if $. == 1;

	chomp $line;
	my ($sp_name, $reg, $seq, $oligo) = split /\t/, $line;
	$sp_count += 1;
	$total_seq += $seq;
	$sp_oligos += $oligo;

	if ($reg eq "Y") {
		$reg_with_seq_count += 1;
		$reg_seq += $seq;
		$reg_oligos += $oligo;

		if ($oligo eq "0") {
			$reg_no_oligo_count += 1;
		} else {
			$reg_with_oligo_count += 1;
		}
	} else {
		if ($oligo eq "0") {
                        $not_reg_no_oligo_count += 1;
                } else {
                        $not_reg_with_oligo_count += 1;
                }
	}
}

if ($total_seq == "0") {
	while (my $line = <$qcf>) {
		next if $line !~ ">";
		
		chomp $line;
		my $gi_species = (split /\|/, $line)[1];
		my $gi = (split /\_/, $gi_species)[0];
		(my $species = $gi_species) =~ s/$gi\_//g;
		$species =~ s/\_/ /g;
		print "sp $species\n";

		if (defined $species) {
			$sp_count += 1;
			$total_seq += 1;
		}	

		if (grep /^$species$/, @reg_list_syn_count) {
			print "found\n";
			$reg_with_seq_count += 1;
			$reg_seq += 1;
		} else {
			print "not found\n";
			print "@reg_list_syn_count";
		}
	}
}

# Output summary
my $summary = "Overall_summary";
open (OUT, ">", $summary);
print (OUT "#_species_with_seq\t#_reg_sp_in_given_list\t#_reg_sp_incl._syn\t#_reg_sp_with_seq\t#_reg_sp_with_oligos\t#_reg_sp_no_oligo\t#_not_reg_sp_with_oligos\t#_not_reg_sp_no_oligo\tTotal_oligos\tsp_level_oligos\tReg_sp_oligos\tTotal_seq\tReg_sp_seq\n");

print (OUT "$sp_count\t$reg_list_count\t", scalar @reg_list_syn_count, "\t$reg_with_seq_count\t$reg_with_oligo_count\t$reg_no_oligo_count\t$not_reg_with_oligo_count\t$not_reg_no_oligo_count\t", scalar @oligos,"\t$sp_oligos\t$reg_oligos\t$total_seq\t$reg_seq\n");

close (OUT);
