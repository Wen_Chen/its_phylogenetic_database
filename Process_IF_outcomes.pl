#!/usr/bin/perl
use warnings;
use strict;
use List::MoreUtils qw(uniq);
use List::MoreUtils qw(indexes);
use Data::Dumper qw(Dumper);

my $input_dir = shift ();
my $out_dir = shift();
my $reg_spe_list = shift();

#my $speciesFile = "$input_dir/test_species";
#my $currentFile = "$input_dir/test_current";
#my $norecordFile = "$input_dir/test_norecord";

my $speciesFile = "$input_dir/$reg_spe_list\_IF_species_xml_parsed.txt";
my $currentFile = "$input_dir/$reg_spe_list\_IF_species_current_name_xml_parsed.txt";
my $norecordFile = "$input_dir/$reg_spe_list\_IF_norecord_final.txt";

my %hash_current;
my %hash_synonym;
my %hash_synonym_current;

# Open all files
open(SF, '<', $speciesFile) or die "Could not open $speciesFile file.";
open(CF, '<', $currentFile) or die "Could not open $currentFile file.";
open(NF, '<', $norecordFile) or die "Could not open $norecordFile file.";

# For those species name in species_current_name_xml_parsed.txt, store Species_Name into hash values with Accepted_Name as key; also create a hash with synonym name as key and current name as value
while (my $line = <CF>) {
	next if $. == 1;

	chomp $line;
	my ($original_sp, $sp_name, $accept_sp) = (split "\t", $line)[0,1,2];
	next if $original_sp eq "Holotype ZT Villacarlos 34-4";

	if ($accept_sp ne '') {
		if (exists $hash_current{$accept_sp}) {
			$hash_current{$accept_sp}[@{$hash_current{$accept_sp}}] = $sp_name; 
		} else {  
			$hash_current{$accept_sp}[0] = $accept_sp;
			$hash_current{$accept_sp}[1] = $sp_name;
		}
		unless (exists $hash_synonym_current{$sp_name}) {
			$hash_synonym_current{$sp_name} = $accept_sp;
		}  
	}
}

close(CF);

$hash_synonym_current{"Null"} = "Null";

#print Dumper \%hash_current;
#print Dumper \%hash_synonym_current;

$| = 1;
print "Current_hash, done\n";

# For those species name in species_xml_parsed.txt, store Species_original, Species_Name, Accepted_Name into hash values with Species_original as key
while (my $line = <SF>) {
	$line =~ /^$/ and next;
	next if $. == 1;

	chomp $line;
	my ($original_sp, $sp_name, $accept_sp) = (split "\t", $line)[0,1,2];
        next if $original_sp eq "Holotype ZT Villacarlos 34-4";

	# if the Accepted name column is blank, ignore unless original_name matches species_name
	if ($accept_sp ne "" || $sp_name eq $original_sp) {
		if ($original_sp !~ /\s/) {		# if there is only Genus name in the field due to spp.
			$original_sp = "$original_sp spp.";
		}	
	
		if (exists $hash_synonym{$original_sp}) {
			$hash_synonym{$original_sp}[@{$hash_synonym{$original_sp}}] = $sp_name;
	                if (exists $hash_current{$accept_sp}) {
				for my $i (0 .. @{$hash_current{$accept_sp}}-1) {
					$hash_synonym{$original_sp}[@{$hash_synonym{$original_sp}}]=$hash_current{$accept_sp}[$i];
				}
			}
	        } else {        
			$hash_synonym{$original_sp}[0] = $original_sp;
			$hash_synonym{$original_sp}[1] = $sp_name;
			if (exists $hash_current{$accept_sp}) {
				for my $i (0 .. @{$hash_current{$accept_sp}}-1) {
					$hash_synonym{$original_sp}[@{$hash_synonym{$original_sp}}]=$hash_current{$accept_sp}[$i];
				}
			}
		}
	}
}
close(SF);

# Store those species in no_record into hash with Null as value
while (my $line = <NF>) {
	chomp $line;
	$hash_synonym{$line}[0] = "Null";
}
close(NF);

#print Dumper \%hash_synonym;
print "Synonym_hash, done\n";

# Open file for output synonym list
my $syn_output = "$out_dir/Synonym_and_current_name_list_from_IF";
open(SYN_OUTFILE, '>', $syn_output);
print (SYN_OUTFILE "Original_name\tSynonym_name\tCurrent_name\n");

# Retrieve all synonym and current name information store in hashs
foreach my $key (sort keys %hash_synonym) {
	my @synonym;
	for my $i (0 .. @{$hash_synonym{$key}}-1) {
		push (@synonym, $hash_synonym{$key}[$i]);
	}

	my @uniq_synonym = uniq @synonym;
	for my $j (0 .. $#uniq_synonym) {
		unless (exists $hash_synonym_current{$uniq_synonym[$j]}) {
			$hash_synonym_current{$uniq_synonym[$j]} = $uniq_synonym[$j];
		}
		print (SYN_OUTFILE "$key\t$uniq_synonym[$j]\t$hash_synonym_current{$uniq_synonym[$j]}\n");
	}
}
	 
close(SYN_OUTFILE);
