#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Process_IF_outcome"
#$ -S /bin/bash

reg_spe_list=$1;
oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

d=`date +%Y%m%d`;

if [ ! -d $raw ]; then
        mkdir $raw;
fi

perl $script/Process_IF_outcomes.pl $oligo/Index_fungorum_Summary/IF_outcome_$d $raw $reg_spe_list

#cd $oligo/Index_fungorum_Summary/
#tar -czvf IF_"$d".tar.gz IF_outcome_$d

#rm -rf $oligo/Index_fungorum_Summary/IF_outcome_$d

