#!/usr/bin/env perl
use warnings;
use strict;
use Bio::SeqIO;
use File::Find;
use File::Spec;
use File::Basename;
use List::MoreUtils qw(first_index);
use List::MoreUtils qw(last_index);
use FindBin;
use lib "$FindBin::Bin/Modules";
use Bio::DB::Taxonomy;
use LWP::Simple;

#author: Eric Hardy; Wen Chen <wen.chen@agr.gc.ca>; Chistine Lowe
if((scalar @ARGV) gt 2 ){
    print("\nUsage: perl rename_genbank_sequences.pl <<Fasta file downloaded from genbank>>\n\n\t\tScript extracts the gi number and looks up the most recent taxonomy for the sequence from entrez,\n\t\t it then appends the identification details from the sequence description\n\t\tScript assumes that the 2nd column of the sequence header will be the gi number\n");
    exit();
}

my $gb_fasta = shift or die "genbank sequences?";
my $lineage = shift();

my $base = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/";
my $taxonomy_db = Bio::DB::Taxonomy->new(-source => 'entrez');
$taxonomy_db->entrez_url($base);

print "Beginning of file $gb_fasta\n";
_reformat($gb_fasta);

my %taxon_lineage;

open (LIN, ">>", $lineage);

foreach my $key (keys %taxon_lineage) {
	print (LIN "$key\t$taxon_lineage{$key}\n");
}

close(LIN);

#changes sequence data into a standard form
sub _reformat {
    my $seq_ref  = shift();
    my @suffix = (".fasta", ".fas", ".fna", ".fa");
    my $basename=basename($seq_ref, @suffix);
    my $gen;
    my $spe;
    
    open (OUT, ">$basename.NE.fasta");
    open (OUT1, ">> $basename.error.txt");
    my @errorID;
    my @goodID;
    my $in=Bio::SeqIO->new(-file=>"$seq_ref", -format=>"fasta");
    my $count=0;
    
    while (my $seq=$in->next_seq()) {
        my $taxon;
        $count++;
   	my $id = $seq->id();
        chomp($id);
	print "$id\n";
        my @col_id = split /[\.]/, $id;
	my $accession = $col_id[0];
	my $scientific_name;
	my $taxonid;
	my $url = $base."efetch.fcgi?db=nuccore&id=$accession&rettype=fasta&retmode=xml";
	my $data = get($url);
	if (! defined $data) {
		return "Not Found";
	}
	$taxonid = (split(/<\/TSeq_taxid>/, (split(/<TSeq_taxid>/, $data))[1]))[0];
	
	my $taxon_list = join("|", "taxonid", $taxonid);
	my $newid = join("_", $accession, $taxon_list);
	
	$scientific_name = (split(/<\/TSeq_orgname>/, (split(/<TSeq_orgname>/, $data))[1]))[0];
        $scientific_name =~ s/\s/_/g;
        $scientific_name =~ s/\.//g;
	
	unless (exists $taxon_lineage{$taxonid}) {
		$taxon_lineage{$taxonid} = getlineage($taxonid);
	}
	
        $newid = join("_", $newid, $scientific_name);
        my $new_seq  = $seq->seq();
        $new_seq  =~ s/-//g;      #remove dashes (sequence alignment gaps)
        
        #shorten and reformat the description to just genus & species name
        my $desc     = $seq->desc();
        my @words    = split(/[ |:,]+/, $desc); # get individual words of description
        #figure out how long the name is supposed to be
        my $name_length = 0;
	 
        if ( $words[0] !~ m/^[\w]+/ ) {
            print OUT1 "Error: ", $id, " ", $desc, "\n";
            push (@errorID, $id);
            #exit;
        } elsif ( $words[0] =~ m/^\w\./ ) {
            print OUT1 "Error: ", $id, " ", $desc, "||";
            print OUT1 "genus name is not in full!", "\n";
            push (@errorID, $id);
            $gen = (split /\./, $words[0])[0];
            $spe = (split /\./, $words[0])[1];
            #exit;
        } else {
            push (@goodID, $id);
            $gen = $words[0];
            $spe = $words[1];
        }
        
        my $new_desc1 = join("_", $gen, $spe);
        $newid = join("|", $newid, $taxonid, $new_desc1);
        
        # obtain strain info, separated by "|"
        my @want = ('var.', 'cf.', 'f.', 'sp.', 'aff.', 'spnov');
        my @want1 = ("strain", "clone",  "isolate", "clade", "vochour", "type", "genotype", "pathotype", "race", 'sect', "ATCC", "AS", 'BMP', "BRIP", "CBS",  'CID', "CCFC", 'CCF', 'CPC', 'CMFISB', "CV", "DAOM", 'DGG', "DTO", 'EEB', 'EGS', "FRR", "IBT", 'ICMP', "IMI", "JCM", "KACC", "LD", 'Mos', 'MAFF', 'NBRC', "NRRL", 'PRPI', 'RGR', 'STE', 'URM');
        my $index;
        my $index_max = $#words;
        my @new_desc2;
        foreach my $search (@want) {
            $index= first_index { $_ eq $search } @words;
            if ($index > 0 ) {
                if ( ($index+2) < $index_max) {
                    push(@new_desc2, join("_", @words[($index)..($index+2)]));
                } else {
                    push(@new_desc2, join("_", @words[($index)..($index_max)]));
                }
            }
        }
        my $new_desc2 = join("_", @new_desc2); # join by "|"
        $newid = join("_", $newid, $new_desc2);
        
        my $index1;
        my @new_desc3;
        foreach my $search1 (@want1) {
            $index1= first_index { $_ eq $search1 } @words;
            if ($index1 > 0 ) {
                if ( ($index1+2) < $index_max) {
                    push(@new_desc3, join("|", @words[($index1)..($index1+2)]));
                } else {
                    push(@new_desc3, join("|", @words[($index1)..($index_max)]));
                }
            } else {
                #push(@new_desc3, "");
            }
        }
        if(@new_desc3){
            my $new_desc3 = join("|", @new_desc3); # join by "|"
            @new_desc3 = split(/\|/, $new_desc3);
            foreach my $test (@new_desc3){
                my $first = first_index { $_ eq $test } @new_desc3;
                my $last = last_index { $_ eq $test } @new_desc3;
                if ($first != $last){
                    splice(@new_desc3, $last, 1);
                }
            }
            $new_desc3 = join("|", @new_desc3); # join by "|"
            $new_desc3 =~ s/^/\|/g;
            $newid = join("_", $newid, $new_desc3);
        }
        my $new_id =  $newid; #new_desc = "<genus>_<species>";
        
        for ($new_id) {
            s/[:]+/:/g;
            #s/[_+()\-;\.\|\*\/ ]+/_/g;
            s/[_+()\-;\.\*\/ ]+/_/g;
            #		s/_18S_ribosomal_RNA_gene_partial_sequence//g;
            #		s/_5\.8S_rRNA_gene_and_internal_transcribed_spacers_1_and_2_ITS1//g;
            s/complete//g;
            s/genes//g;
            s/gene//g;
            s/_for_//g;
            s/18S//g;
            s/rRNA//g;
            s/internal//g;
            s/_5_8S//g;
            s/transcribed//g;
            s/spacer//g;
            s/partial//g;
            s/RNA//g;
            s/sequence//g;
            s/ribosomal//g;
            s/small//g;
            s/subunit//g;
            s/ribosomal//g;
            s/and//g;
            s/_+/_/g;
            s/_+$//g;
            s/_isolate$//g;
            s/:_/:/g;
            s/\|_/\|/g;
            s/[\W]+$//g;
            s/^\|//g;
            s/[_]+/_/g;                   #remove multiple "_";
            s/_+$//g;                    #remove any trailing "_";
            s/[\|]+/\|/g;
            s/[\|]+$/_/g;
	    s/,//g;
	    s/'//g;
	    s/\[//g;
	    s/\]//g;
        }
        
        chomp($new_id);
        $new_id =~ s/_\|/\|/g;
        $new_id =~ s/\|$//g;
        $new_id =~ s/culture_collection//g;
        $new_id =~ s/strain/st/g;
        $new_id =~ s/genotype/gt/g;
        $new_id =~ s/isolate/iso/g;
        print OUT ">",$new_id, "\n", $seq->seq, "\n";
        
    }
    
}

# Get lineage from taxonID
sub getlineage {
	my $taxonID = shift();
	my $rank = "";
	my $taxon;
	my $ancestor;
	my $k = "";
	my $p = "";
	my $c = "";
	my $o = "";
	my $f = "";
	my $g = "";
	my $s = "";

	while ($rank ne "superkingdom") {
		$taxon = $taxonomy_db->get_taxon(-taxonid => $taxonID);
		$rank = $taxon->rank;
		my $sci = $taxon->scientific_name();
		if ($rank eq "species") {
			$s = $sci;
		} elsif ($rank eq "genus") {
			$g = $sci;
		} elsif ($rank eq "family") {
			$f = $sci;
                } elsif ($rank eq "order") {
			$o = $sci;
                } elsif ($rank eq "class") {
			$c = $sci;
                } elsif ($rank eq "phylum") {
			$p = $sci;
                } elsif ($rank eq "kingdom") {
			$k = $sci;
                }

		$ancestor = $taxonomy_db->ancestor($taxon);
		
		$taxonID = $ancestor->id;
	}

	return "k__$k; p__$p; c__$c; o__$o; f__$f; g__$g; s__$s";
}
