#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Download_rename"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";

d=`date +%Y%m%d_%H%M%S`;
temp="oligo_database_update_temp_$d";
user=$1;

ssh wenchenaafc@10.117.203.189 "mkdir ~/$temp";
sleep 10;
scp -r $script/local_download_rename.sh wenchenaafc@10.117.203.189:~/$temp;
sleep 10;
ssh wenchenaafc@10.117.203.189 "~/$temp/local_download_rename.sh $oligo $temp $user";

