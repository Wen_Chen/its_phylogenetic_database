#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Generate_query"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

if [ ! -d "$raw/download_seq" ]; then
        mkdir $raw/download_seq
fi

perl $script/download_seq/Download_Seqs/create_query.pl $raw/Query_genus_list '[orgn] AND ((internal[All Fields] AND transcribed[All Fields] AND spacer[All Fields]) OR ITS1[All Fields] OR ITS2[All Fields]) NOT (supercontig[All Fields] OR (complete[All Fields] AND genome[All Fields]) OR chromosome[All Fields]) NOT srcdb refseq[prop] NOT wgs[keyword] NOT tsa[keyword] NOT gbdiv pat[prop]' $raw/download_seq/genus.query

