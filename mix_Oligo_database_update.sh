#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Oligo_database_update"
#$ -S /bin/bash

oligo=${1%/}
reg_spe_list=$2
user=$3
script="$oligo/its_phylogenetic_database"
raw="$oligo/raw"

$script/change_paths.sh $oligo

qsub $script/qsub_IF.sh $reg_spe_list

qsub -hold_jid IndexFungorum $script/qsub_Process_IF_outcome.sh $reg_spe_list

qsub -hold_jid Process_IF_outcome $script/qsub_Generate_mapping_file.sh

qsub -hold_jid Generate_mapping_file $script/qsub_generate_query.sh

qsub -hold_jid Generate_query $script/qsub_local_download_rename.sh $user

