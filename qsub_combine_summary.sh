#!/bin/bash
#$ -q all.q
#$ -pe smp 1
#$ -cwd
#$ -N "Combine_summary"
#$ -S /bin/bash

oligo="PATH_TO_DATABASE";
script="$oligo/its_phylogenetic_database";
raw="$oligo/raw";

rm -rf $raw/*/*_folder_arrangement.p*

echo -e "Genus\t#_species_with_seq\t#_reg_sp_in_given_list\t#_reg_sp_incl._syn\t#_reg_sp_with_seq\t#_reg_sp_with_oligos\t#_reg_sp_no_oligo\t#_not_reg_sp_with_oligos\t#_not_reg_sp_no_oligo\tTotal_oligos\tsp_level_oligos\tReg_sp_oligos\tTotal_seq\tReg_sp_seq" > $raw/Oligo_database_summary.csv


cat $raw/Original_genus_list | while read genus;
        do
	info=$(awk 'NR==2' $raw/$genus/Summary/Overall_summary);
	echo -e "$genus\t$info" >> $raw/Oligo_database_summary.csv
done;


